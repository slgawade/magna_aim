﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './Components/Login/Login.component';
import { MasterComponent } from './Components/Master/MASTER.component';
import { DashboardComponent } from './Components/Dashboard/Dashboard.component';
import { REPORTSComponent } from './Components/REPORTS/REPORTS.component';
import { VIN_DetailsComponent } from './Components/REPORTS/GENEALOGY/VIN_Details/VIN_Details.component';
import { BirthCertificateComponent } from './Components/REPORTS/GENEALOGY/BirthCertificate/BirthCertificate.component';
import { ShipmentAuditorComponent } from './Components/REPORTS/MATERIAL/ShipmentAuditor/ShipmentAuditor.component';
import { ShipmentDetailsComponent } from './Components/REPORTS/MATERIAL/ShipmentAuditor/shipment-details/shipment-details.component';
import { InventoryComponent } from './Components/REPORTS/MATERIAL/inventory/inventory.component';
import { UnshippedComponent } from './Components/REPORTS/MATERIAL/unshipped/unshipped.component';
import { QualityscrapcountComponent } from './Components/REPORTS/PRODUCTION/qualityscrapcount/qualityscrapcount.component';
import { UnitgenealogyreportComponent } from './Components/REPORTS/GENEALOGY/unitgenealogyreport/unitgenealogyreport.component';
import { ProductioncycleComponent } from './Components/REPORTS/PRODUCTION/productioncycle/productioncycle.component';
import { demoComponent } from './Components/demo/demo.component';
import { ShipmentFemComponent } from './Components/REPORTS/MATERIAL/SHIPMENT/shipment-fem/shipment-fem.component';
import { ShipmentGrilleComponent } from './Components/REPORTS/MATERIAL/SHIPMENT/shipment-grille/shipment-grille.component';
import { ShipmentHeaderComponent } from './Components/REPORTS/MATERIAL/SHIPMENT/shipment-header/shipment-header.component';
import { BypassComponent } from './Components/REPORTS/PRODUCTION/bypass/bypass.component';
import { BuildpartlookupComponent } from './Components/REPORTS/BROADCAST/buildpartlookup/buildpartlookup.component';
import { PphourComponent } from './Components/REPORTS/DASHBOARD/pphour/pphour.component';
import { PpoperatorComponent } from './Components/REPORTS/DASHBOARD/ppoperator/ppoperator.component';
import { QuerypartlookupComponent } from './Components/REPORTS/BROADCAST/querypartlookup/querypartlookup.component';
import { BufferFemComponent } from './Components/ANDON/buffer-fem/buffer-fem.component';
import { BufferGrilleHeaderComponent } from './Components/ANDON/buffer-grille-header/buffer-grille-header.component';
import { RawDataQueryComponent } from './Components/REPORTS/BROADCAST/raw-data-query/raw-data-query.component';
import { WipstatusComponent } from './Components/REPORTS/PRODUCTION/wipstatus/wipstatus.component';
import { PabdiscrepancyComponent } from './Components/REPORTS/BROADCAST/pabdiscrepancy/pabdiscrepancy.component';
import { PendingGqComponent } from './Components/REPORTS/BROADCAST/pending-gq/pending-gq.component';
import { PendingSrComponent } from './Components/REPORTS/BROADCAST/pending-sr/pending-sr.component';
import { BatchBuildComponent } from './Components/REPORTS/GENEALOGY/batch-build/batch-build.component';
import { QueueSummaryComponent } from './Components/REPORTS/BROADCAST/queue-summary/queue-summary.component';
import { ProductionSummaryComponent } from './Components/REPORTS/BROADCAST/production-summary/production-summary.component';
import { GrilleShippingComponent } from './Components/ANDON/grille-shipping/grille-shipping.component';
import { T1xxContainerDetailsComponent } from './Components/REPORTS/MATERIAL/t1xx-container-details/t1xx-container-details.component';


const appRoutes: Routes = [
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'Master',
        component: MasterComponent,
        children: [
            {
                path: 'demo',
                component: demoComponent
            },
            {
                path: '',
                component: DashboardComponent,
            },
            {
                path: 'Dashboard',
                component: DashboardComponent,
            },
            {
                path: 'REPORTS',
                component: REPORTSComponent,
                children: [
                    {
                        path: 'VIN_Details',
                        component: VIN_DetailsComponent
                    },
                    {
                        path: 'BirthCertificate',
                        component: BirthCertificateComponent
                    },
                    {
                        path: 'BatchBuild',
                        component: BatchBuildComponent
                    },
                    {
                        path: 'demo',
                        component: demoComponent
                    },
                    {
                        path: 'ShipmentAuditor',
                        component: ShipmentAuditorComponent
                    },
                    {
                        path: 'ShipmentDetails',
                        component: ShipmentDetailsComponent
                    },
                    {
                        path: 'WIPStatus',
                        component: WipstatusComponent
                    },
                    {
                        path: 'Inventory',
                        component: InventoryComponent
                    },
                    {
                        path: 'UnShipped',
                        component: UnshippedComponent
                    },
                    {
                        path: 'QualityScrapCount',
                        component: QualityscrapcountComponent
                    },
                    {
                        path: 'UnitGenealogyReport',
                        component: UnitgenealogyreportComponent
                    },
                    {
                        path: 'Productioncycle',
                        component: ProductioncycleComponent
                    },
                    {
                        path: 'Shipment_FEM',
                        component: ShipmentFemComponent
                    },
                    {
                        path: 'Shipment_GRILLE',
                        component: ShipmentGrilleComponent
                    },
                    {
                        path: 'Shipment_HEADER',
                        component: ShipmentHeaderComponent
                    },
                    {
                        path: 'Bypass',
                        component: BypassComponent
                    },
                    {
                        path: 'BuildPartLookUp',
                        component: BuildpartlookupComponent

                    },
                    {
                        path: 'PartsPerOperator',
                        component: PpoperatorComponent
                    },
                    {
                        path: 'PartsPerHour',
                        component: PphourComponent
                    },
                    {
                        path: 'PartLookUp',
                        component: QuerypartlookupComponent
                    },
                    {
                        path: 'RawDataQuery',
                        component: RawDataQueryComponent
                    },
                    {
                        path: 'PABdiscrepancy',
                        component: PabdiscrepancyComponent
                    },
                    {
                        path: 'PendingGQ',
                        component: PendingGqComponent
                    },
                    {
                        path: 'PendingSR',
                        component: PendingSrComponent
                    },
                    {
                        path: 'QueueSummary',
                        component: QueueSummaryComponent
                    },
                    {
                        path: 'ProductionSummary',
                        component: ProductionSummaryComponent
                    },
                    {
                        path: 'T1xxContainerDetails',
                        component: T1xxContainerDetailsComponent
                    }
                       
                ]
            }
        ]

    },
    {
        path: 'FEM',
        component: BufferFemComponent
    },
    {
        path: "GRILLE_HEADER",
        component: BufferGrilleHeaderComponent
    },
    {
        path: "GRILLE_SHIPPING",
        component: GrilleShippingComponent
    }

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: true });