﻿
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalService } from '../../app.component';

@Injectable()
export class testService {
    api: string;
    constructor(private http: Http, private global: GlobalService) {
        this.api = global.Url + 'DataCollectionHistory';
    }

    getdata(inputDetails) {
        return this.http.post(this.api, inputDetails).map(response =>
            response.json());
    }

}