﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DragulaService } from 'ng2-dragula';
declare var $: any;
declare var _: any;


@Component({
    selector: 'dashboard',
    templateUrl: `./Dashboard.html`,
    styleUrls: ['./Dashboard.css'],

})
export class DashboardComponent {

    constructor(private router: Router, private dragulaService: DragulaService) {
        //dragulaService.setOptions('first-bag', {
        //    removeOnSpill: true
        //});
    }
    ngOnInit() {
       
    }
    ngAfterViewInit() {

    }

    ngAfterViewChecked() {
       
    }
    ngOnDestroy() {

    }

}