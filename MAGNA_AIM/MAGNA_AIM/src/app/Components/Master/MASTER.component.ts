﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, URLSearchParams } from "@angular/http";
import { MasterService } from '../../Services/Master.service';
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var _: any;
declare var screenfull: any;

@Component({
    selector: 'master',
    templateUrl: `./Master.html`,
    styleUrls: ['./Master.css'],
    host: {
        '(document:click)': 'handleClick($event)',
    }

})
export class MasterComponent {
    //@ViewChild(DataTableDirective)
    //dtElement: DataTableDirective;

    datasource = [];
    FilterData: any;
    Filter_InInterval: any;

    constructor(private router: Router,
        private masterService: MasterService) {
      
    }
    ngOnInit() {

    }
   
    handleClick(event) {
        //console.log($(event.target).attr('data-name'));
        if ($(event.target).attr('data-name') != 'menu')
            $("#nav").slideUp('fast');
    }
    ngAfterViewInit() {

    }
    ngAfterViewChecked() {

    }
    ngOnDestroy() {

    }

}
@Component({
    selector: 'mstheader',
    templateUrl: `./header.html`,
    styleUrls: ['./Master.css']
})
export class HeaderComponent {
    clock: any;
    constructor() {
        setInterval(() => {
            this.clock = moment(new Date()).format("hh:mm:ss A");
        },1000);
       
    }
    ShowNavigation() {
        $("#ul_menu").slideToggle();
        console.log('ShowNavigation');
    }
    HideNavigation() {

        console.log('HideNavigation');
    }
    fullScreen(id) {
        console.log(id);
        if (screenfull.enabled) {
            screenfull.toggle()
        }
    }
    ngOnInit() {

    }
    ngAfterViewChecked() {

    }
    toggleMenu() {
        $("#nav").slideToggle('fast');
    }
}
@Component({
    selector: 'mstfooter',
    templateUrl: `./footer.html`,
    styleUrls: ['./Master.css']
})
export class FooterComponent {
    constructor() {

    }
    ngOnInit() {

    }
    ngAfterViewChecked() {

    }

}