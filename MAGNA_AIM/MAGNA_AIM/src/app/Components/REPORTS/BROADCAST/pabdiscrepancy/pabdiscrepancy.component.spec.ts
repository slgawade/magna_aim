import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PabdiscrepancyComponent } from './pabdiscrepancy.component';

describe('PabdiscrepancyComponent', () => {
  let component: PabdiscrepancyComponent;
  let fixture: ComponentFixture<PabdiscrepancyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PabdiscrepancyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PabdiscrepancyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
