import { Component, OnInit } from '@angular/core';

import { MasterService } from '../../../../Services/Master.service';
import { FilterService } from '../../../../Services/filter.service';
import { Http, URLSearchParams } from "@angular/http";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var require: any;

@Component({
    selector: 'app-querypartlookup',
    templateUrl: './querypartlookup.component.html',
    styleUrls: ['./querypartlookup.component.css']
})
export class QuerypartlookupComponent implements OnInit {

    Lines = []; Parts = [];
    lineName: string = ""; lookuppart: string = "";
    constructor(private masterService: MasterService,
        private filterService: FilterService,
        private daterangepickerOptions: DaterangepickerConfig) { }


    ngOnInit() {
        $("#mainSecondDiv").css('padding', '0px');
        this.masterService.StartLoader();
        this.getData("line");
    }
    getData(mode) {
        if (mode == "line") {
            let FilterParamData = new URLSearchParams();
            FilterParamData.append("mode", mode);
            this.Lines = [];
            this.filterService.getLines(FilterParamData).subscribe(response => {
                console.log('route');
                console.log(response);

                for (let i in response) {
                    this.Lines.push({
                        name: response[i].name
                    })
                }
                console.log(this.Lines);
                this.masterService.StopLoader();
            });
        }
        else if (mode == "lookuppart") {

            let ff = new URLSearchParams();
            ff.append("mode", mode);
            ff.append("type", this.lineName);
            ff.append("startDate", moment(this.dateInputs[0].start).format('YYYY-MM-DD'));
            ff.append("endDate", moment(this.dateInputs[0].end).format('YYYY-MM-DD') +" 23:59:59");
            this.Parts = [];
            console.log(ff);
            this.filterService.getLines(ff).subscribe(response => {
                console.log('lookuppart');
                console.log(response);

                for (let i in response) {
                    this.Parts.push({
                        name: response[i].name
                    })
                }
                console.log(this.Parts);
                this.masterService.StopLoader();
            });
        }
    }
    ngOnDestroy() {
        $("#mainSecondDiv").css('padding', '5px 10px 10px 10px');
    }
    sendParameterToReport() {
        this.masterService.StartLoader();
        let Url = this.masterService.getFTVPURL();
        let sdate = moment(this.dateInputs[0].start).format('YYYY-MM-DD');
        let edate = moment(this.dateInputs[0].end).format('YYYY-MM-DD');

        console.log(Url);
        console.log("this.lineName :" + this.lineName);

        let src = Url + "%2fProduction_Reports%2fQueryPartLookup&line=" + this.lineName + "&start=" + sdate + "&end=" + edate + " 23:59:59&partNum=" + this.lookuppart;
        console.log(src);
        $("#querypartlookup_Iframe").attr("src", src);
    }
    oniFrameLoad(e) {
        let src = $(e.currentTarget).attr('src');

        if (src != undefined) {
            this.masterService.StopLoader();
        }

    }
    filterClick(ulName) {
        $("#" + ulName).slideToggle();
    }
    Itemclick(e) {

        let filterID = $(e.currentTarget).attr('data-ulid');
        //let index = $(e.currentTarget).attr('data-index');
        if ($(e.currentTarget).attr('data-name') == "LINE") {
            this.lineName = $(e.currentTarget).attr('data-nm');
            this.dateInputs = [];;
            this.dateInputs.push(
                {
                    start: moment().subtract(2, 'days'),
                    end: moment(),
                    mode: ''
                });
            this.lookuppart = "";
            this.Parts = [];
        }
        else if ($(e.currentTarget).attr('data-name') == "PART")
            this.lookuppart = $(e.currentTarget).attr('data-nm');

        if ($(e.currentTarget).children('a').children('i').hasClass('fa-circle-o')) {
            $("#" + filterID + " li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });
            $(e.currentTarget).children('a').children('i').removeClass('fa-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-check-circle-o');
        }
        else {
            $("#" + filterID + " li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });

            $(e.currentTarget).children('a').children('i').removeClass('fa-check-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-circle-o');
        }


        console.log(this.lineName);
        console.log(this.lookuppart);
    }
    openNav() {
        var ft = document.getElementById("filterToggle").className;

        if (ft == "fa fa-angle-right") {
            document.getElementById("mySidenav").style.width = "300px";
            document.getElementById("mainsub").style.marginLeft = "300px";
            document.getElementById("mainsub").style.transition = "all 0.5s";

            document.getElementById("filterToggle").classList.remove('fa-angle-right');
            document.getElementById("filterToggle").classList.add('fa-angle-left');
        }
        else {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("mainsub").style.marginLeft = "0";
            document.getElementById("mainsub").style.transition = "all 0.5s";
            document.getElementById("filterToggle").classList.remove('fa-angle-left');
            document.getElementById("filterToggle").classList.add('fa-angle-right');
        }
    }
    onkeypress(event) {
        console.log($(event.currentTarget).val());
        let ind, input, filter, ul, li, a, i;
        filter = $(event.currentTarget).val().toUpperCase();
        ul = document.getElementById($(event.currentTarget).attr('data-name'));
        li = ul.getElementsByTagName("li");
        for (i = 1; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.text.toUpperCase().indexOf(filter) > -1) {
                $(li[i]).css('display', 'block');
            } else {
                $(li[i]).css('display', 'none')

            }
        }
    }
    DatePickerOptions() {
        this.daterangepickerOptions.settings = {
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false,
            autoUpdateInput: false,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 3 Months': [moment().subtract(4, 'month'), moment()],
                'Last 6 Months': [moment().subtract(6, 'month'), moment()],
                'Last 12 Months': [moment().subtract(12, 'month'), moment()],
            }
        };
    }
    public dateInputs: any = [
        {
            start: moment().subtract(2, 'days'),
            end: moment(),
            mode: ''
        }
    ];
    private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        dateInput.mode = '1';
        console.log("selectedDate: " + dateInput.start + ":" + dateInput.end);
        this.masterService.StartLoader();
        this.getData("lookuppart");
    }



}
