import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuerypartlookupComponent } from './querypartlookup.component';

describe('QuerypartlookupComponent', () => {
  let component: QuerypartlookupComponent;
  let fixture: ComponentFixture<QuerypartlookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuerypartlookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuerypartlookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
