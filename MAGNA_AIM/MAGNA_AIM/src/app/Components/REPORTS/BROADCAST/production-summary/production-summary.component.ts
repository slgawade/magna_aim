import { Component, OnInit } from '@angular/core';

import { MasterService } from '../../../../Services/Master.service';
import { FilterService } from '../../../../Services/filter.service';
import { Http, URLSearchParams } from "@angular/http";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var require: any;

@Component({
    selector: 'app-production-summary',
    templateUrl: './production-summary.component.html',
    styleUrls: ['./production-summary.component.css']
})
export class ProductionSummaryComponent implements OnInit {

    a_Lines = []; a_Shifts = [];
    lineName: string = ""; shift: string = "";
    constructor(private masterService: MasterService,
        private filterService: FilterService, private daterangepickerOptions: DaterangepickerConfig) { }

    ngOnInit() {
        $("#mainSecondDiv").css('padding', '0px');
        this.masterService.StartLoader();
        this.getData();

    }
    getData() {

        this.a_Lines = [
            { name: 'FEM', id: 'X83' },
            { name: 'GRILLE', id: 'X79' },
            { name: 'HEADER', id: 'Header' },
            { name: 'Carrier', id: 'Carrier' },
            { name: 'AGS Upper', id: 'AGS Upper' },
            { name: 'AGS Lower', id: 'AGS Lower' },
            { name: 'AAD', id: 'AAD' },
            { name: 'T1XX_GMC', id: 'T1XX_GMC' },
            { name: 'T1XX_LOWER', id: 'T1XX_LOWER' },
            { name: 'T1XX_CHEVY', id: 'T1XX_CHEVY' }
        ];

        this.a_Shifts = [{ name: '1', id: '1' }, { name: '2', id: '2' }];
        this.masterService.StopLoader();


    }

    ngOnDestroy() {
        $("#mainSecondDiv").css('padding', '5px 10px 10px 10px');
    }
    sendParameterToReport() {
        this.masterService.StartLoader();
        let Url = this.masterService.getFTVPURL();

        console.log(Url);
        console.log("this.lineName :" + this.lineName);
        console.log("this.shift :" + this.shift);

        let fromDate = moment(this.dateInputs[0].start).format('YYYY-MM-DD');
        let toDate = moment(this.dateInputs[0].end).format('YYYY-MM-DD');

        let src = Url + "%2fProduction_Reports%2fProductionSummary&Line=" + this.lineName + "&shift=" + this.shift + "&fromDate=" + fromDate + "&toDate=" + toDate;
        console.log(src);

        $("#prodsummaryreport_Iframe").attr("src", src);
    }
    oniFrameLoad(e) {
        let src = $(e.currentTarget).attr('src')

        if (src != undefined) {
            this.masterService.StopLoader();
        }

    }
    filterClick(ulName) {
        $("#" + ulName).slideToggle();
    }

    onkeypress(event) {
        console.log($(event.currentTarget).val());
        let ind, input, filter, ul, li, a, i;
        filter = $(event.currentTarget).val().toUpperCase();
        ul = document.getElementById($(event.currentTarget).attr('data-name'));
        li = ul.getElementsByTagName("li");
        for (i = 1; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.text.toUpperCase().indexOf(filter) > -1) {
                $(li[i]).css('display', 'block');
            } else {
                $(li[i]).css('display', 'none')

            }
        }
    }

    Itemclick(e) {

        let filterID = $(e.currentTarget).attr('data-ulid');

        if ($(e.currentTarget).attr('data-name') == "LINE") {
            this.lineName = $(e.currentTarget).attr('data-nm');
        }
        else if ($(e.currentTarget).attr('data-name') == "SHIFT") {
            this.shift = $(e.currentTarget).attr('data-nm');
        }


        if ($(e.currentTarget).children('a').children('i').hasClass('fa-circle-o')) {
            $("#" + filterID + " li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });
            $(e.currentTarget).children('a').children('i').removeClass('fa-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-check-circle-o');
        }
        else {
            $("#" + filterID + " li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });

            $(e.currentTarget).children('a').children('i').removeClass('fa-check-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-circle-o');
        }


    }

    openNav() {
        var ft = document.getElementById("filterToggle").className;

        if (ft == "fa fa-angle-right") {
            document.getElementById("mySidenav").style.width = "300px";
            document.getElementById("mainsub").style.marginLeft = "300px";
            document.getElementById("mainsub").style.transition = "all 0.5s";

            document.getElementById("filterToggle").classList.remove('fa-angle-right');
            document.getElementById("filterToggle").classList.add('fa-angle-left');
        }
        else {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("mainsub").style.marginLeft = "0";
            document.getElementById("mainsub").style.transition = "all 0.5s";
            document.getElementById("filterToggle").classList.remove('fa-angle-left');
            document.getElementById("filterToggle").classList.add('fa-angle-right');
        }
    }

    DatePickerOptions() {
        this.daterangepickerOptions.settings = {
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 3 Months': [moment().subtract(4, 'month'), moment()],
                'Last 6 Months': [moment().subtract(6, 'month'), moment()],
                'Last 12 Months': [moment().subtract(12, 'month'), moment()],
            }
        };
    }
    public dateInputs: any = [
        {
            start: moment().subtract(2, 'days'),
            end: moment(),
        }
    ];
    private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
    }

}
