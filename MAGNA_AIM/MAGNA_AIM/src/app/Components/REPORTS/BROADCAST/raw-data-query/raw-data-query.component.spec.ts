import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawDataQueryComponent } from './raw-data-query.component';

describe('RawDataQueryComponent', () => {
  let component: RawDataQueryComponent;
  let fixture: ComponentFixture<RawDataQueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RawDataQueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawDataQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
