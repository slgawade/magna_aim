import { Component, OnInit } from '@angular/core';

import { MasterService } from '../../../../Services/Master.service';
import { FilterService } from '../../../../Services/filter.service';
import { RawDataQueryService } from '../../../../Services/REPORTS/RawDataQuery.service';
import { Http, URLSearchParams } from "@angular/http";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var require: any;

@Component({
    selector: 'app-raw-data-query',
    templateUrl: './raw-data-query.component.html',
    styleUrls: ['../.././REPORTS.css', './raw-data-query.component.css'],
    providers: [RawDataQueryService]
})
export class RawDataQueryComponent implements OnInit {

    Lines = []; VINs = []; Broadcasts = []; Sequences = [];
    lineName: string = ""; Broadcast: string = ""; Sequence: string = ""; VIN: string = "";
    flag: boolean = false;
    RawData = ""; R_MessageType = ""; R_PlantCode = ""; R_SupplierCode = ""; R_PartNumber = "";
    R_Last8ofVIN = ""; R_ModelCode = ""; R_ModuleType = ""; R_StatusCode = ""; R_SequenceNo = ""; R_TransmissionType = "";

    constructor(private masterService: MasterService,
        private filterService: FilterService,
        private daterangepickerOptions: DaterangepickerConfig,
        private rawDataQueryService: RawDataQueryService) { }


    ngOnInit() {
        $("#btnSubmit").css('display', 'none');
        $("#rawdata").css('display', 'none');
        $("#mainSecondDiv").css('padding', '0px');
        this.masterService.StartLoader();
        this.getData();
    }
    getData() {

        this.Lines = [{ name: 'FEM', id: 'X83' }, { name: 'GRILLE', id: 'X79' }];
        this.Broadcasts = [{ name: 'E' }, { name: 'G' }];
        this.masterService.StopLoader();

    }
    ngOnDestroy() {
        $("#mainSecondDiv").css('padding', '5px 10px 10px 10px');
    }
    sendParameterToReport() {

        this.flag = false;

        this.ClearValues();
        this.masterService.StartLoader();

        this.Sequence = $("#filter_Sequence").val();
        this.VIN = $("#filter_VIN").val();

        if (this.Sequence == '')
            this.Sequence = "0";
        if (this.VIN == '')
            this.VIN = "0";

        console.log("this.lineName :" + this.lineName);
        console.log("this.Broadcast : " + this.Broadcast);
        console.log("this.Sequence :" + this.Sequence);
        console.log("this.VIN : " + this.VIN);

        let FilterParamData = new URLSearchParams();
        FilterParamData.append("cmdText", "MAGNA_RawDataQuery");
        FilterParamData.append("mode", "main");
        FilterParamData.append("ModuleType_S", this.lineName);
        FilterParamData.append("StatusCode_S", this.Broadcast);
        FilterParamData.append("SequenceNumber_I", this.Sequence);
        FilterParamData.append("VIN_S", this.VIN);

        this.rawDataQueryService.getRawDataQuery(FilterParamData).subscribe(res => {
            console.log('response');
            console.log(res);

            if (res.length > 0) {
                this.RawData = res[0].message_S;

                this.R_Last8ofVIN = res[1].last_8_Of_VIN;
                this.R_MessageType = res[1].message_Type;
                this.R_ModelCode = res[1].model_Code;
                this.R_ModuleType = res[1].module_Type;
                this.R_PlantCode = res[1].plant_Code;
                this.R_SequenceNo = res[1].sequence_Number;
                this.R_StatusCode = res[1].status_Code;
                this.R_SupplierCode = res[1].supplier_Code;
                this.R_TransmissionType = res[1].transmission_Type;

                for (let i in res) {
                    if (res[i].partNumber != undefined)
                        this.R_PartNumber += res[i].partNumber + ", ";
                }
               
                this.R_PartNumber = this.R_PartNumber.substring(0, this.R_PartNumber.length - 2);

                $("#rawdata").css('display', 'block');
                $("#divParsed").css('display', 'none');
                $("#btnSubmit").css('display', 'block');
            }
            else {
                this.flag = true;
            }
            this.masterService.StopLoader();
        });


    }
    ClearValues() {
        this.RawData = "";

        this.R_Last8ofVIN = "";
        this.R_MessageType = "";
        this.R_ModelCode = "";
        this.R_ModuleType = "";
        this.R_PlantCode = "";
        this.R_SequenceNo = "";
        this.R_StatusCode = "";
        this.R_SupplierCode = "";
        this.R_TransmissionType = "";
        this.R_PartNumber = "";
        $("#btnSubmit").css('display', 'none');
        $("#rawdata").css('display', 'none');
        $("#divParsed").css('display', 'none');
    }
    filterClick(ulName) {
        $("#" + ulName).slideToggle();
    }
    showParsed() {
        $("#divParsed").fadeIn('slow');
    }

    Itemclick(e) {

        let filterID = $(e.currentTarget).attr('data-ulid');
        //let index = $(e.currentTarget).attr('data-index');
        if ($(e.currentTarget).attr('data-name') == "LINE")
            this.lineName = $(e.currentTarget).attr('data-nm');
        else if ($(e.currentTarget).attr('data-name') == "BROADCAST")
            this.Broadcast = $(e.currentTarget).attr('data-nm');


        if ($(e.currentTarget).children('a').children('i').hasClass('fa-circle-o')) {
            $("#" + filterID + " li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });
            $(e.currentTarget).children('a').children('i').removeClass('fa-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-check-circle-o');
        }
        else {
            $("#" + filterID + " li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });

            $(e.currentTarget).children('a').children('i').removeClass('fa-check-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-circle-o');
        }


        console.log(this.lineName);
        console.log(this.Broadcast);
    }
    openNav() {
        var ft = document.getElementById("filterToggle").className;

        if (ft == "fa fa-angle-right") {
            document.getElementById("mySidenav").style.width = "300px";
            document.getElementById("mainsub").style.marginLeft = "300px";
            document.getElementById("mainsub").style.transition = "all 0.5s";

            document.getElementById("filterToggle").classList.remove('fa-angle-right');
            document.getElementById("filterToggle").classList.add('fa-angle-left');
        }
        else {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("mainsub").style.marginLeft = "0";
            document.getElementById("mainsub").style.transition = "all 0.5s";
            document.getElementById("filterToggle").classList.remove('fa-angle-left');
            document.getElementById("filterToggle").classList.add('fa-angle-right');
        }
    }
    onkeypress(event) {
        console.log($(event.currentTarget).val());
        let ind, input, filter, ul, li, a, i;
        filter = $(event.currentTarget).val().toUpperCase();
        ul = document.getElementById($(event.currentTarget).attr('data-name'));
        li = ul.getElementsByTagName("li");
        for (i = 1; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.text.toUpperCase().indexOf(filter) > -1) {
                $(li[i]).css('display', 'block');
            } else {
                $(li[i]).css('display', 'none')

            }
        }
    }

}
