import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueueSummaryComponent } from './queue-summary.component';

describe('QueueSummaryComponent', () => {
  let component: QueueSummaryComponent;
  let fixture: ComponentFixture<QueueSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueueSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
