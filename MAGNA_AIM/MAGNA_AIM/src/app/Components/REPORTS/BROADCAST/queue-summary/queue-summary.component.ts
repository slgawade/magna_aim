import { Component, OnInit } from '@angular/core';

import { MasterService } from '../../../../Services/Master.service';
import { FilterService } from '../../../../Services/filter.service';
import { Http, URLSearchParams } from "@angular/http";
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var require: any;

@Component({
    selector: 'app-queue-summary',
    templateUrl: './queue-summary.component.html',
    styleUrls: ['./queue-summary.component.css']
})
export class QueueSummaryComponent implements OnInit {

    a_Lines = []; a_StatusCodes = []; a_Variants = [];
    lineName: string = ""; statusCode: string = ""; variant: string = "";
    constructor(private masterService: MasterService, private filterService: FilterService) { }

    ngOnInit() {
        $("#mainSecondDiv").css('padding', '0px');
        this.masterService.StartLoader();
        this.getData('');
       
    }
    getData(mode) {
        if (mode == '') {
            this.a_Lines = [{ name: 'FEM', id: 'X83' }, { name: 'GRILLE', id: 'X79' }, { name: 'HEADER', id: 'HEADER' }];
            this.a_StatusCodes = [{ name: 'E', id: 'E' }, { name: 'G', id: 'G' }];
            this.masterService.StopLoader();
        }
        else if (mode == "variant") {
            let ff = new URLSearchParams();
            ff.append("mode", mode);
            ff.append("type", this.lineName);

            this.a_Variants = [];

            this.filterService.getLines(ff).subscribe(response => {
                console.log('variant');
                console.log(response);

                for (let i in response) {
                    this.a_Variants.push({
                        name: response[i].name
                    })
                }
                console.log(this.a_Variants);
                this.masterService.StopLoader();
            });
        }


    }

    ngOnDestroy() {
        $("#mainSecondDiv").css('padding', '5px 10px 10px 10px');
    }
    sendParameterToReport() {
        this.masterService.StartLoader();
        let Url = this.masterService.getFTVPURL();

        console.log(Url);
        console.log("this.lineName :" + this.lineName);
        console.log("this.statusCode :" + this.statusCode);
        console.log("this.variant :" + this.variant);

        let src = Url + "%2fProduction_Reports%2fQueueSummaryReport&Line=" + this.lineName + "&StatusCode=" + this.statusCode + "&Variant=" + this.variant;
        console.log(src);

        $("#queuesummaryreport_Iframe").attr("src", src);
    }
    oniFrameLoad(e) {
        let src = $(e.currentTarget).attr('src')

        if (src != undefined) {
            this.masterService.StopLoader();
        }

    }
    filterClick(ulName) {
        $("#" + ulName).slideToggle();
    }

    onkeypress(event) {
        console.log($(event.currentTarget).val());
        let ind, input, filter, ul, li, a, i;
        filter = $(event.currentTarget).val().toUpperCase();
        ul = document.getElementById($(event.currentTarget).attr('data-name'));
        li = ul.getElementsByTagName("li");
        for (i = 1; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.text.toUpperCase().indexOf(filter) > -1) {
                $(li[i]).css('display', 'block');
            } else {
                $(li[i]).css('display', 'none')

            }
        }
    }

    Itemclick(e) {


        let filterID = $(e.currentTarget).attr('data-ulid');

        if ($(e.currentTarget).attr('data-name') == "LINE") {
            this.lineName = $(e.currentTarget).attr('data-nm');
            this.getData('variant');
        }
        else if ($(e.currentTarget).attr('data-name') == "STATUSCODE") {
            this.statusCode = $(e.currentTarget).attr('data-nm');
        }
        else if ($(e.currentTarget).attr('data-name') == "VARIANT") {
            this.variant = $(e.currentTarget).attr('data-nm');
        }

        if ($(e.currentTarget).children('a').children('i').hasClass('fa-circle-o')) {
            $("#" + filterID + " li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });
            $(e.currentTarget).children('a').children('i').removeClass('fa-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-check-circle-o');
        }
        else {
            $("#" + filterID + " li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });

            $(e.currentTarget).children('a').children('i').removeClass('fa-check-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-circle-o');
        }
     
       
    }

    openNav() {
        var ft = document.getElementById("filterToggle").className;

        if (ft == "fa fa-angle-right") {
            document.getElementById("mySidenav").style.width = "300px";
            document.getElementById("mainsub").style.marginLeft = "300px";
            document.getElementById("mainsub").style.transition = "all 0.5s";

            document.getElementById("filterToggle").classList.remove('fa-angle-right');
            document.getElementById("filterToggle").classList.add('fa-angle-left');
        }
        else {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("mainsub").style.marginLeft = "0";
            document.getElementById("mainsub").style.transition = "all 0.5s";
            document.getElementById("filterToggle").classList.remove('fa-angle-left');
            document.getElementById("filterToggle").classList.add('fa-angle-right');
        }
    }

}
