import { Component, OnInit } from '@angular/core';

import { MasterService } from '../../../../Services/Master.service';
import { FilterService } from '../../../../Services/filter.service';
import { Http, URLSearchParams } from "@angular/http";
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var require: any;

@Component({
  selector: 'app-pending-sr',
  templateUrl: './pending-sr.component.html',
  styleUrls: ['./pending-sr.component.css']
})
export class PendingSrComponent implements OnInit {

    Lines = [];
    lineName: string = "";
    constructor(private masterService: MasterService, private filterService: FilterService) { }

    ngOnInit() {
        $("#mainSecondDiv").css('padding', '0px');
        this.masterService.StartLoader();
        this.getData();
    }
    getData() {
        this.Lines = [{ name: 'FEM', id: 'X83' }, { name: 'GRILLE', id: 'X79' }];
        this.masterService.StopLoader();
    }

    ngOnDestroy() {
        $("#mainSecondDiv").css('padding', '5px 10px 10px 10px');
    }
    sendParameterToReport() {
        this.masterService.StartLoader();
        let Url = this.masterService.getFTVPURL();

        console.log(Url);
        console.log("this.lineName :" + this.lineName);

        let src = Url + "%2fProduction_Reports%2fPendingSR&line=" + this.lineName;
        console.log(src);
        $("#pendingsr_Iframe").attr("src", src);
    }
    oniFrameLoad(e) {
        let src = $(e.currentTarget).attr('src')

        if (src != undefined) {
            this.masterService.StopLoader();
        }

    }
    filterClick(ulName) {
        $("#" + ulName).slideToggle();
    }
    onkeypress(event) {
        console.log($(event.currentTarget).val());
        let ind, input, filter, ul, li, a, i;
        filter = $(event.currentTarget).val().toUpperCase();
        ul = document.getElementById($(event.currentTarget).attr('data-name'));
        li = ul.getElementsByTagName("li");
        for (i = 1; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.text.toUpperCase().indexOf(filter) > -1) {
                $(li[i]).css('display', 'block');
            } else {
                $(li[i]).css('display', 'none')

            }
        }
    }
    Itemclick(e) {

        this.lineName = $(e.currentTarget).attr('data-nm');

        if ($(e.currentTarget).children('a').children('i').hasClass('fa-circle-o')) {
            $("#Line_list li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });
            $(e.currentTarget).children('a').children('i').removeClass('fa-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-check-circle-o');
        }
        else {
            $("#Line_list li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });

            $(e.currentTarget).children('a').children('i').removeClass('fa-check-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-circle-o');
        }


        console.log(this.lineName);
    }

    openNav() {
        var ft = document.getElementById("filterToggle").className;

        if (ft == "fa fa-angle-right") {
            document.getElementById("mySidenav").style.width = "300px";
            document.getElementById("mainsub").style.marginLeft = "300px";
            document.getElementById("mainsub").style.transition = "all 0.5s";

            document.getElementById("filterToggle").classList.remove('fa-angle-right');
            document.getElementById("filterToggle").classList.add('fa-angle-left');
        }
        else {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("mainsub").style.marginLeft = "0";
            document.getElementById("mainsub").style.transition = "all 0.5s";
            document.getElementById("filterToggle").classList.remove('fa-angle-left');
            document.getElementById("filterToggle").classList.add('fa-angle-right');
        }
    }

}
