import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingSrComponent } from './pending-sr.component';

describe('PendingSrComponent', () => {
  let component: PendingSrComponent;
  let fixture: ComponentFixture<PendingSrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingSrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingSrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
