import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildpartlookupComponent } from './buildpartlookup.component';

describe('BuildpartlookupComponent', () => {
  let component: BuildpartlookupComponent;
  let fixture: ComponentFixture<BuildpartlookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildpartlookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildpartlookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
