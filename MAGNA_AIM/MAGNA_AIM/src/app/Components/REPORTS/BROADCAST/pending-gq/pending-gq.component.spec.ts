import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingGqComponent } from './pending-gq.component';

describe('PendingGqComponent', () => {
  let component: PendingGqComponent;
  let fixture: ComponentFixture<PendingGqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingGqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingGqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
