import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnshippedComponent } from './unshipped.component';

describe('UnshippedComponent', () => {
  let component: UnshippedComponent;
  let fixture: ComponentFixture<UnshippedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnshippedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnshippedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
