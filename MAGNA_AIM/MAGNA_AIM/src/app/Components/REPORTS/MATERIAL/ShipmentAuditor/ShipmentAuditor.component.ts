import { Component, OnInit } from '@angular/core';
import { Http, URLSearchParams } from "@angular/http";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MasterService } from '../../../../Services/Master.service';
import { ShipmentAuditorService } from '../../../../Services/REPORTS/ShipmentAuditor.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { LocalStorageService } from 'angular-2-local-storage';

import * as moment from 'moment';
declare var $: any;
declare var _: any;

@Component({
    selector: 'ShipmentAuditor',
    templateUrl: './ShipmentAuditor.component.html',
    styleUrls: ['./ShipmentAuditor.component.css'],
    providers: [ShipmentAuditorService]
})
export class ShipmentAuditorComponent implements OnInit {

    ResponseData: any;
    ParentDatasource: any;
    ChildDatasource: any;
    SubChildDatasource: any;
    FilterData: any;
    tableData: any;


    constructor(private shipmentAuditorService: ShipmentAuditorService,
        private masterService: MasterService,
        private daterangepickerOptions: DaterangepickerConfig,
        private LS: LocalStorageService,
        private router: Router) {

    }

    ngOnInit() {
      
    }

    sendParamData() {
        
        let ModuleType = $("#ddl_ModuleType").val();
        let sdate = moment(this.dateInputs[0].start).format('YYYY-MM-DD');
        let edate = moment(this.dateInputs[0].end).format('YYYY-MM-DD');

        if (ModuleType == 'none')
            $("#ddl_ModuleType").css('border-color', 'red');
        else {
            $("#ddl_ModuleType").css('border-color', '#ccc');

            this.masterService.StartLoader();

            let FilterParamData = new URLSearchParams();
            FilterParamData.append("cmdText", "MAGNA_SA_ShipNumber");
            FilterParamData.append("startDate", sdate);
            FilterParamData.append("endDate", edate);
            FilterParamData.append("line", ModuleType);

            console.log(FilterParamData);
            this.getData(FilterParamData);
        }
    }

    getData(FilterData: any) {
        this.shipmentAuditorService.getShipmentAuditor(FilterData).subscribe(response => {
            console.log('response');
            console.log(response);
            this.ResponseData = response;
            this.setData(response);
        });

    }
    setData(response) {
        this.ParentDatasource = []; this.ChildDatasource = []; this.SubChildDatasource = [];
        this.masterService.clearTable("tbl_ShipmentAuditor");
        this.ParentDatasource = [];
        if (response.length > 0) {

            this.ParentDatasource = response;

            console.log('this.ParentDatasource'); console.log(this.ParentDatasource);

            this.setDatatable(this.ParentDatasource);
            this.tableData = $('#tbl_ShipmentAuditor').dataTable();
            this.tableData.fnAddData(this.ParentDatasource, false);
            this.tableData.fnDraw();
            //}
        }

        this.masterService.StopLoader();
    }
    setDatatable(DataSource: any) {
        let table = $('#tbl_ShipmentAuditor').DataTable({
            paginationType: 'full_numbers',
            destroy: true,
            displayLength: 10,
            //data: DataSource,
            deferRender: true,
            //dom: 'Bfrtip',
            responsive: true,
            scrollCollapse: true,
            scrollX: true,
            scrollY: '400px',
            sort: false,
            fixedHeader: true,
            //fixedColumns: true,
            paging: true,
            columns: [
                { title: "ID", data: "$id", visible: false },
                { title: "SHIPPER NUMBER", data: "shipper_Number_S" },
                { title: "MODULE TYPE", data: "moduleType_S" },
                { title: "TRUCK NUMBER", data: "truck_Number_S" },
                { title: "SHIPMENT DATE", data: "shipment_Date_T" },

            ],
            rowCallback: (row: Node, data: any[] | Object, index: number) => {
                const self = this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', row).unbind('click');
                $('td', row).bind('click', () => {
                    this.rowClick(data);
                });
                return row;
            }

        });
        console.log("setDatatable");
        $("#tbl_ShipmentAuditor_length").css('display', 'none');
        $("#tbl_ShipmentAuditor_filter").css('display', 'none');
    }
    rowClick(e) {
        console.log(e);

        this.LS.set('ShipperNumber', e.shipper_Number_S);
        this.LS.set('ModuleType', e.moduleType_S);

        this.router.navigateByUrl("/Master/REPORTS/ShipmentDetails");
    }

    DatePickerOptions() {
        this.daterangepickerOptions.settings = {
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 3 Months': [moment().subtract(4, 'month'), moment()],
                'Last 6 Months': [moment().subtract(6, 'month'), moment()],
                'Last 12 Months': [moment().subtract(12, 'month'), moment()],
            }
        };
    }
    public dateInputs: any = [
        {
            start: moment().subtract(2, 'days'),
            end: moment()
        }
    ];
    private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
    }

}
