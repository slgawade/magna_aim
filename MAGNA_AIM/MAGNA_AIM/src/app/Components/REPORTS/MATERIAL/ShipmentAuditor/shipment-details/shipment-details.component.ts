import { Component, OnInit } from '@angular/core';
import { MasterService } from '../../../../../Services/Master.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, URLSearchParams } from "@angular/http";
import { LocalStorageService } from 'angular-2-local-storage';
import { ShipmentAuditorService } from '../../../../../Services/REPORTS/ShipmentAuditor.service';
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var require: any;
declare var _: any;

@Component({
    selector: 'app-shipment-details',
    templateUrl: './shipment-details.component.html',
    styleUrls: ['../../.././REPORTS.css', './shipment-details.component.css'],
    providers: [ShipmentAuditorService]
})
export class ShipmentDetailsComponent implements OnInit {
    PartNumber_ds = []; VINs_ds = []; SR_ds = []; vin: string; srmessage: string;
    P_dataSource = []; V_dataSource = []; S_dataSource = [];
    constructor(private masterService: MasterService,
        private shipmentAuditorService: ShipmentAuditorService,
        private LS: LocalStorageService) {
        this.masterService.StartLoader();
        this.getData(this.LS.get('ShipperNumber'), this.LS.get('ModuleType'))
    }

    ngOnInit() {

    }
    getData(ShipperNumber, ModuleType) {
        let FilterParamData = new URLSearchParams();
        FilterParamData.append("cmdText", "MAGNA_SA_PartNumber");
        FilterParamData.append("ShipperNumber", ShipperNumber);
        FilterParamData.append("line", ModuleType);

        this.PartNumber_ds = [];
        this.shipmentAuditorService.getShipmentAuditor(FilterParamData).subscribe(response => {
            console.log('response');
            console.log(response);
            for (let i in response) {
                if (response[i].value == 'PartNumber') {
                    this.PartNumber_ds.push(response[i]);
                }
                if (response[i].value == 'VINs') {
                    this.VINs_ds.push(response[i]);
                }
                if (response[i].value == 'SR') {
                    this.SR_ds.push(response[i]);
                }
            }

            this.setData(this.PartNumber_ds, this.VINs_ds, this.SR_ds, this.LS.get('ShipperNumber'));
        });
    }
    setData(P_ds, V_ds,S_ds, ShipperNumber) {
        let message = "";
        for (let i in P_ds) {
            if (P_ds[i].shipper_Number_S == ShipperNumber) {
                this.V_dataSource = [];
                for (let j in V_ds) {
                    if (V_ds[j].partNumber_S == P_ds[i].partNumber_S && V_ds[j].shipper_Number_S == P_ds[i].shipper_Number_S) {
                        message = "";
                        for (let k in S_ds) {
                            if (V_ds[j].viN_S == S_ds[k].viN_S && V_ds[j].moduleType_S == S_ds[k].moduleType_S) {
                                message = S_ds[k].rawSRMessage_S;
                                break;
                            }
                        }
                        this.V_dataSource.push({
                            partNumber_S: V_ds[j].partNumber_S,
                            viN_S: V_ds[j].viN_S,
                            rawSRMessage_S: message
                        });
                    }
                }
                this.P_dataSource.push({
                    partNumber_S: P_ds[i].partNumber_S,
                    cnt: P_ds[i].cnt,
                    childDetails: this.V_dataSource
                });
            }
        }
        console.log(this.P_dataSource);
        this.masterService.StopLoader();
    }
    showSrMsg(msg, vin) {
        this.srmessage = msg;
        this.vin = vin;

        $('#modal_Shipment').modal('show');
    }
    showchild(event, parentValue, ind) {
        console.log(parentValue);

        $("[data-hide='" + parentValue + ind + "']").slideToggle('slow');

        if ($(event.currentTarget).children('a').children('i').hasClass('fa-chevron-down')) {
            $(event.currentTarget).children('a').children('i').removeClass('fa-chevron-down')
            $(event.currentTarget).children('a').children('i').addClass('fa-chevron-up');
        }
        else {
            $(event.currentTarget).children('a').children('i').removeClass('fa-chevron-up')
            $(event.currentTarget).children('a').children('i').addClass('fa-chevron-down');
        }

        // alert('bhau');
    }
    ngOnDestroy() {
        this.LS.set('flag', 'true');
    }
}
