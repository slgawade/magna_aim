import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipmentHeaderComponent } from './shipment-header.component';

describe('ShipmentHeaderComponent', () => {
  let component: ShipmentHeaderComponent;
  let fixture: ComponentFixture<ShipmentHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipmentHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipmentHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
