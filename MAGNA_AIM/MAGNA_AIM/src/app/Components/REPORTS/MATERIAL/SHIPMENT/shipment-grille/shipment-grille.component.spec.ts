import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipmentGrilleComponent } from './shipment-grille.component';

describe('ShipmentGrilleComponent', () => {
  let component: ShipmentGrilleComponent;
  let fixture: ComponentFixture<ShipmentGrilleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipmentGrilleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipmentGrilleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
