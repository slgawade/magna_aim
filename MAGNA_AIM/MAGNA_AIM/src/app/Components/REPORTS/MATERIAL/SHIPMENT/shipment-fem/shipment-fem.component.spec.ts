import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipmentFemComponent } from './shipment-fem.component';

describe('ShipmentFemComponent', () => {
  let component: ShipmentFemComponent;
  let fixture: ComponentFixture<ShipmentFemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipmentFemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipmentFemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
