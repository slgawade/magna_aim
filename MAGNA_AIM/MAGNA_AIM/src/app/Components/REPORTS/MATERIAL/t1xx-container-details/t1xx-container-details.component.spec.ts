import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { T1xxContainerDetailsComponent } from './t1xx-container-details.component';

describe('T1xxContainerDetailsComponent', () => {
  let component: T1xxContainerDetailsComponent;
  let fixture: ComponentFixture<T1xxContainerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ T1xxContainerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(T1xxContainerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
