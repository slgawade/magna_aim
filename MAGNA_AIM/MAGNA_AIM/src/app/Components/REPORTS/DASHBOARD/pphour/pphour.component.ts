import { Component, OnInit } from '@angular/core';
import { MasterService } from '../../../../Services/Master.service';
import { FilterService } from '../../../../Services/filter.service';
import { Http, URLSearchParams } from "@angular/http";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var require: any;

@Component({
  selector: 'app-pphour',
  templateUrl: './pphour.component.html',
  styleUrls: ['./pphour.component.css']
})
export class PphourComponent implements OnInit {

    Lines = [];
    lineName: string = "";
    constructor(private masterService: MasterService,
        private filterService: FilterService,
        private daterangepickerOptions: DaterangepickerConfig) { }
    getData() {
        let FilterParamData = new URLSearchParams();
        FilterParamData.append("mode", "route");

        this.filterService.getLines(FilterParamData).subscribe(response => {
            console.log('response');
            console.log(response);

            for (let i in response) {
                this.Lines.push({
                    name: response[i].name
                })
            }
            console.log(this.Lines);
            this.masterService.StopLoader();
        });

    }
    ngOnInit() {
        $("#mainSecondDiv").css('padding', '0px');
        this.masterService.StartLoader();
        this.getData();
    }
    ngOnDestroy() {
        $("#mainSecondDiv").css('padding', '5px 10px 10px 10px');
    }
    sendParameterToReport() {
        this.masterService.StartLoader();
        let Url = this.masterService.getFTVPURL();
        let sdate = moment(this.dateInputs[0].start).format('YYYY-MM-DD');
        let edate = moment(this.dateInputs[0].end).format('YYYY-MM-DD');

        console.log(Url);
        console.log("this.lineName :" + this.lineName);

        let src = Url + "%2fProduction_Reports%2fPPPH&line=" + this.lineName + "&fromDate=" + sdate + "&toDate=" + edate +" 23:59:59";
        console.log(src);
        $("#pphour_Iframe").attr("src", src);
    }
    oniFrameLoad(e) {
        let src = $(e.currentTarget).attr('src');

        if (src != undefined) {
            this.masterService.StopLoader();
        }

    }
    filterClick(ulName) {
        $("#" + ulName).slideToggle('slow');
    }
    Itemclick(e) {

        //let filterID = $(e.currentTarget).attr('data-filterID');
        //let index = $(e.currentTarget).attr('data-index');

        this.lineName = $(e.currentTarget).attr('data-nm');

        if ($(e.currentTarget).children('a').children('i').hasClass('fa-circle-o')) {
            $("#Line_list li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });
            $(e.currentTarget).children('a').children('i').removeClass('fa-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-check-circle-o');
        }
        else {
            $("#Line_list li").each(function (index) {
                $(this).children('a').children('i').removeClass();
                $(this).children('a').children('i').addClass('iconColor fa fa-circle-o')
            });

            $(e.currentTarget).children('a').children('i').removeClass('fa-check-circle-o');
            $(e.currentTarget).children('a').children('i').addClass('fa-circle-o');
        }


        console.log(this.lineName);
    }
    onkeypress(event) {
        console.log($(event.currentTarget).val());
        let ind, input, filter, ul, li, a, i;
        filter = $(event.currentTarget).val().toUpperCase();
        ul = document.getElementById($(event.currentTarget).attr('data-name'));
        li = ul.getElementsByTagName("li");
        for (i = 1; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.text.toUpperCase().indexOf(filter) > -1) {
                $(li[i]).css('display', 'block');
            } else {
                $(li[i]).css('display', 'none')

            }
        }
    }
    openNav() {
        var ft = document.getElementById("filterToggle").className;

        if (ft == "fa fa-angle-right") {
            document.getElementById("mySidenav").style.width = "300px";
            document.getElementById("mainsub").style.marginLeft = "300px";
            document.getElementById("mainsub").style.transition = "all 0.5s";

            document.getElementById("filterToggle").classList.remove('fa-angle-right');
            document.getElementById("filterToggle").classList.add('fa-angle-left');
        }
        else {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("mainsub").style.marginLeft = "0";
            document.getElementById("mainsub").style.transition = "all 0.5s";
            document.getElementById("filterToggle").classList.remove('fa-angle-left');
            document.getElementById("filterToggle").classList.add('fa-angle-right');
        }
    }
    DatePickerOptions() {
        this.daterangepickerOptions.settings = {
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 3 Months': [moment().subtract(4, 'month'), moment()],
                'Last 6 Months': [moment().subtract(6, 'month'), moment()],
                'Last 12 Months': [moment().subtract(12, 'month'), moment()],
            }
        };
    }
    public dateInputs: any = [
        {
            start: moment().subtract(2, 'days'),
            end: moment()
        }
    ];
    private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
    }
}
