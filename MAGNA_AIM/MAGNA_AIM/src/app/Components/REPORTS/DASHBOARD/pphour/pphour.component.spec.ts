import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PphourComponent } from './pphour.component';

describe('PphourComponent', () => {
  let component: PphourComponent;
  let fixture: ComponentFixture<PphourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PphourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PphourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
