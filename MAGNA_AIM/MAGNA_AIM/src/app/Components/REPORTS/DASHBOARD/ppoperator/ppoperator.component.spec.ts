import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PpoperatorComponent } from './ppoperator.component';

describe('PpoperatorComponent', () => {
  let component: PpoperatorComponent;
  let fixture: ComponentFixture<PpoperatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PpoperatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PpoperatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
