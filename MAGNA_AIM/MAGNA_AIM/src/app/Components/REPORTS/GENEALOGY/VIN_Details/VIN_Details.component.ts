﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, URLSearchParams } from "@angular/http";
import { AmChartsService } from "@amcharts/amcharts3-angular";
import { MasterService } from '../../../../Services/Master.service';
import { VIN_DetailsService } from '../../../../Services/REPORTS/VIN_Details.service';
import { EmailService } from '../../../../Services/REPORTS/Email.service';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { DaterangePickerComponent } from 'ng2-daterangepicker';
//import { CoolLocalStorage } from 'angular2-cool-storage';
import { LocalStorageService } from 'angular-2-local-storage';
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var _: any;
declare var screenfull: any;
declare var require: any;
declare var jsPDF: any;

@Component({
    selector: 'VIN_Details',
    templateUrl: `./VIN_Details.html`,
    styleUrls: ['../.././REPORTS.css', './VIN_Details.css'],
    providers: [VIN_DetailsService, EmailService]

})
export class VIN_DetailsComponent {
    //@ViewChild(DataTableDirective)
    //dtElement: DataTableDirective;

    chart: any;
    chartdata: any;
    datasource = [];
    DataColumns = [];
    dateFlag: boolean = false;
    FilterData: any;
    Filter_InInterval: any;
    static router1: Router;
    tableData: any;

    constructor(private router: Router,
        private AmCharts: AmChartsService,
        private masterService: MasterService,
        private vin_DetailsService: VIN_DetailsService,
        private emailService: EmailService,
        private LS: LocalStorageService,
        private daterangepickerOptions: DaterangepickerConfig

        //private CS: CoolLocalStorage
    ) {

       

      
    }

    ngOnInit() {

        this.masterService.getData().subscribe(data => {
            this.masterService.filter_INFlag = true;
            this.FilterData = [];
            this.FilterData = data;
        });
        this.masterService.updateTitle("VIN Details");
        this.refreshData();
        this.DatePickerOptions();
    }

    searchData() {
        let dWhere: string = "";
        let line = $("#ddl_ModuleType").val();
        let date = moment(this.dateInputs[0].start).format('YYYY-MM-DD');

        if (line == "none")
            line = "";
        else
            dWhere = dWhere + " ModuleType_S='" + line + "' and Status_S in ('CLOSED','COMPLETED','BUFFER','INSTALLED') and";

        if ($("#txt_SequenceNumber").val() != "")
            dWhere = dWhere + " SequenceNumber_S='" + $("#txt_SequenceNumber").val() + "' and";

        if ($("#txt_VIN").val() != "")
            dWhere = dWhere + " VIN_S='" + $("#txt_VIN").val() + "' and";

        if (this.dateFlag)
            dWhere = dWhere + " CAST(creation_time as date)='" + date + "' and";

        dWhere = dWhere.substring(0, dWhere.length - 3);

        console.log(dWhere);
        let pData = new URLSearchParams();
        pData.append("mode", "filter");
        pData.append("dWhere", dWhere);


        this.masterService.StartLoader();
        this.getData(pData);
    }
    resetFilter() {
        this.LS.set("SequenceNumber_I", "");
        this.LS.set("ModuleType_S", "");
        this.LS.set("VIN_S", "");

        $("#txt_SequenceNumber").val("");
        $("#txt_VIN").val("");
        this.dateFlag = false;
        this.dateInputs = [
            {
                start: moment().add(1, 'days'),
                date: 'YYYY-MM-DD'

            }];

        $("#ddl_ModuleType option")[0].selected = true;
      
    }
    onChange(ele) {
        if (ele == "Line") {
            $("#txt_SequenceNumber").val("");
            $("#txt_VIN").val("");
        }
        else if (ele == "Sequence") {
            this.dateFlag = false;
            $("#txt_VIN").val("");
            this.dateInputs = [
                {
                    start: moment().format('YYYY-MM-DD'),
                    date: 'YYYY-MM-DD'

                }];

            $("#ddl_ModuleType option")[0].selected = true;
        }
        else if (ele == "VIN") {
            this.dateFlag = false;
            $("#txt_SequenceNumber").val("");
            this.dateInputs = [
                {
                    start: moment().format('YYYY-MM-DD'),
                    date: 'YYYY-MM-DD'

                }];

            $("#ddl_ModuleType option")[0].selected = true;
        }
    }
    refreshData() {
        this.resetFilter();
        this.masterService.StartLoader();
        let pData = new URLSearchParams();
        pData.append("mode", "load");

        this.getData(pData);
    }

    reInitialiseDataTable(SearchData) {
        this.masterService.clearTable("tbl_VIN_Details");
        this.setDatatable(SearchData);
        if (SearchData.length > 0) {
            this.tableData = $('#tbl_VIN_Details').dataTable();
            this.tableData.fnAddData(SearchData, false);
            this.tableData.fnDraw();
        }
    }

    getData(FilterData: any) {


        this.vin_DetailsService.getVIN_Details(FilterData).subscribe(response => {
            console.log('response');
            console.log(response);

            this.setData(response);
        });

    }
    setData(response) {
        this.masterService.clearTable("tbl_VIN_Details");
        this.datasource = [];
        if (response.length > 0) {
            this.setDatatable(response);
            this.tableData = $('#tbl_VIN_Details').dataTable();
            this.datasource = response;
            // if (this.datasource.length > 0) {
            this.tableData.fnAddData(response, false);
            this.tableData.fnDraw();
            //}
        }
        this.masterService.updateChildData(this.datasource);
        this.masterService.StopLoader();
    }
    setDatatable(DataSource: any) {
        let table = $('#tbl_VIN_Details').DataTable({
            paginationType: 'full_numbers',
            destroy: true,
            displayLength: 10,
            //data: DataSource,
            deferRender: true,
            //dom: 'Bfrtip',
            responsive: true,
            scrollCollapse: true,
            scrollX: true,
            scrollY: '400px',
            sort: false,
            fixedHeader: true,
            //fixedColumns: true,
            paging: true,
            columns: [
                { title: "ID", data: "$id", visible: false },
                { title: "VIN", data: "viN_S" },
                { title: "ORDER NUMBER", data: "orderNumber_S" },
                { title: "SEQUENCE NUMBER", data: "sequenceNumber_S" },
                { title: "STATUS", data: "status_S" },
                { title: "MODULE TYPE", data: "moduleType_S" },
                { title: "CREATION TIME", data: "creation_time" }

            ],
            rowCallback: (row: Node, data: any[] | Object, index: number) => {
                const self = this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', row).unbind('click');
                $('td', row).bind('click', () => {
                    this.rowClick(data);;
                });
                return row;
            }

        });
        console.log("setDatatable");
        $("#tbl_VIN_Details_length").css('display', 'none');
        $("#tbl_VIN_Details_filter").css('display', 'none');
    }

    DatePickerOptions() {
        this.daterangepickerOptions.settings = {
            autoUpdateInput: false,
            singleDatePicker: true,
            //showDropdowns: true,
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false
        };
    }
    public dateInputs: any = [
        {
            start: '',//moment(new Date()).add(1, 'days'),
            date: 'YYYY-MM-DD'
        }
    ];
    private selectedDate(value: any, dateInput: any) {
        debugger;
        dateInput.start = moment(value.start).format('YYYY-MM-DD');
        dateInput.date = moment(value.start).format('YYYY-MM-DD');
        this.dateFlag = true;
        $("#txt_SequenceNumber").val("");
        $("#txt_VIN").val("");

    }


    ngAfterViewInit() {
        // debugger;
    }

    rowClick(e) {
        console.log(e);

        this.LS.set("SequenceNumber_I", e.sequenceNumber_S);
        this.LS.set("ModuleType_S", e.moduleType_S);
        this.LS.set("VIN_S", e.viN_S);

        this.router.navigateByUrl("/Master/REPORTS/BirthCertificate");

    }

    ngAfterViewChecked() {
        // debugger;
        $("#tbl_VIN_Details_length").css('display', 'none');
        $("#tbl_VIN_Details_filter").css('display', 'none');
    }

    ngOnDestroy() {
        //clearInterval(this.Filter_InInterval);
    }

    setChart(response: any) {

    }

    createChart(dataprovider) {

    }
}