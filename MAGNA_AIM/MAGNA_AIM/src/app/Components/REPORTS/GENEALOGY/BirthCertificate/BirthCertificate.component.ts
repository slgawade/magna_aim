﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, URLSearchParams } from "@angular/http";
import { AmChartsService } from "@amcharts/amcharts3-angular";
import { MasterService } from '../../../../Services/Master.service';
import { BirthCertificateService } from '../../../../Services/REPORTS/BirthCertificate.service';
import { EmailService } from '../../../../Services/REPORTS/Email.service';
import { LocalStorageService } from 'angular-2-local-storage';

import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
//import * as jsPDF from 'jspdf';
//import * as html2canvas from 'html2canvas';
declare var $: any;
declare var require: any;
declare var _: any;
declare var jsPDF: any;
declare var alasql: any;

//let jsPDF = require('jspdf');
//require('jspdf-autotable');


@Component({
    selector: 'BirthCertificate',
    templateUrl: `./BirthCertificate.html`,
    styleUrls: ['../.././REPORTS.css', './BirthCertificate.css'],
    providers: [BirthCertificateService, EmailService]

})
export class BirthCertificateComponent {
    //@ViewChild(DataTableDirective)
    //dtElement: DataTableDirective;

    chart: any;
    VIN: string;
    chartdata: any;
    mail_Receiver: string;
    mail_Subject: string;
    mail_Body: string;
    mail_Time: string;
    base64textString: string = "";
    datasource = [];
    DataColumns = [];
    FilterData: any;
    Filter_InInterval: any;
    HeaderData = [];
    modelType: string;
    yposition: any;
    EOLCameraImagesPath: string;
    M_BroadcastInfo = []; M_SA = []; M_SR = []; M_ComponentsUsed = []; M_Retriggers = []; M_BadgeScans = [];
    M_LabelPrinting = []; M_BYpass = []; M_EOL_T_C = []; M_Shipments = []; Pdf_MCO = []; M_MCO = []; arrTemp = [];
    M_AGS1 = []; M_AGS2 = []; M_AGS3 = []; M_ECM1 = []; M_ECM2 = []; M_ECM3 = []; M_Carrier = []; M_Carrier2 = []; M_Operator = [];
    M_EOLCOmpletion = []; M_EOLCamera = []; arrEOL = []; M_GRILLE = []; M_Torque = []; M_KIT = []; M_HoodLatch = [];

    constructor(private router: Router,
        private AmCharts: AmChartsService,
        private masterService: MasterService,
        private birthCertificateService: BirthCertificateService,
        private emailService: EmailService,
        private LS: LocalStorageService
        //private CS: CoolLocalStorage
    ) {
        //console.log(this.CS.getObject('Filter'));
        //let FilterParamData = new URLSearchParams();
        //FilterParamData.append("SequenceNumber_I", "20171001460");
        //FilterParamData.append("ModuleType_S", "X79");
        //FilterParamData.append("VIN_S", "KN500340");
        this.EOLCameraImagesPath = 'http://172.18.75.117/EOLCameraImages/';

        this.mail_Time = moment(new Date()).format("hh:mm");
        this.modelType = this.LS.get("ModuleType_S").toString();
        this.masterService.StartLoader();
        this.HeaderData.push({
            SequenceNumber_I: this.LS.get("SequenceNumber_I"),
            ModuleType_S: this.LS.get("ModuleType_S"),
            VIN_S: this.LS.get("VIN_S")
        });
        let FilterParamData = new URLSearchParams();
        FilterParamData.append("SequenceNumber_I", this.LS.get("SequenceNumber_I").toString());
        FilterParamData.append("ModuleType_S", this.LS.get("ModuleType_S").toString());
        FilterParamData.append("VIN_S", this.LS.get("VIN_S").toString());

        this.getData(FilterParamData);

        this.VIN = this.masterService.VIN;

        this.Filter_InInterval = setInterval(() => {
            if (this.masterService.filter_INFlag) {
                this.masterService.filter_INFlag = false;
                this.getData(this.FilterData);
                console.log(this.FilterData);
            }
        }, 5);
    }
    ngOnInit() {
        this.setClockPicker();
        this.masterService.getData().subscribe(data => {

            this.masterService.filter_INFlag = true;
            this.FilterData = [];
            this.FilterData = data;
        });
        this.masterService.updateTitle("Birth Certificate");
    }
    setClockPicker() {

        $("#divClock").clockpicker({
            placement: 'bottom',
            align: 'left',
            donetext: 'Done',
            afterDone: () => {
                console.log('done');
            }
        });
    }

    PrintData() {
        let doc = this.getJSPDFdata();
        doc.autoPrint();
        //window.open(doc.output('bloburl'), '_blank');
        window.open(doc.output('bloburl'), "_blank");
    }

    ScheduleMail() {
        let doc = this.getJSPDFdata();

        let pdfData = doc.output();
        pdfData ? btoa(pdfData) : "";

        console.log(this.mail_Receiver);
        console.log(this.mail_Subject);
        console.log(this.mail_Body);
        console.log($("#txt_Clock").val());

        let MailDetails = new URLSearchParams();
        MailDetails.append("sender", "shamsundar.gawade@mestechservices.com");
        MailDetails.append("password", "Sujata@7858");
        MailDetails.append("receiver", this.mail_Receiver);
        MailDetails.append("subject", this.mail_Subject);
        MailDetails.append("body", this.mail_Body);
        MailDetails.append("port", "25");
        MailDetails.append("host", "mail.office365.com");
        MailDetails.append("attachment", pdfData);
        MailDetails.append("time", $("#txt_Clock").val());
        this.emailService.ScheduleMail(MailDetails).subscribe(response => {
            console.log(response);
        })

    }

    ExportXLSX() {
        let opts = [], data = [];
        if (this.M_BroadcastInfo.length > 0) {
            opts.push({
                sheetid: 'BroadcastInfo', header: true,
                column: [
                    { columnid: 'moduleType_S', title: 'Module Type' },
                    { columnid: 'sequenceNumber_I', title: 'Sequence Number' },
                    { columnid: 'creation_time', title: 'Creation Time' },
                    { columnid: 'statusCode_S', title: 'Status Code' }
                ]
            });
            data.push(this.M_BroadcastInfo);
        }
        if (this.M_SA.length > 0) {
            opts.push({ sheetid: 'SA Message', header: true });
            data.push(this.M_SA);
        }
        if (this.M_SR.length > 0) {
            opts.push({ sheetid: 'SR Message', header: true });
            data.push(this.M_SR);
        }
        if (this.M_ComponentsUsed.length > 0) {
            opts.push({ sheetid: 'Components Used', header: true });
            data.push(this.M_ComponentsUsed);
        }
        //if (this.M_MCO.length > 0) {
        //    opts.push({ sheetid: 'MCO', header: true });
        //    data.push(this.M_MCO);
        //}
        if (this.M_LabelPrinting.length > 0) {
            opts.push({ sheetid: 'Label Printing', header: true });
            data.push(this.M_LabelPrinting);
        }
        if (this.M_BYpass.length > 0) {
            opts.push({ sheetid: 'Bypass', header: true });
            data.push(this.M_BYpass);
        }

        //save XlSX
        alasql('SELECT INTO XLSX("BirthCertificate.xlsx",?) FROM ?',
            [
                opts, data
            ]
        );

        console.log('opts');
        console.log(opts);
        console.log('data');
        console.log(data);
    }

    convertToDataURLviaCanvas(url, outputFormat) {
        return new Promise((resolve, reject) => {
            let img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function () {
                let canvas = <HTMLCanvasElement>document.createElement('CANVAS'),
                    ctx = canvas.getContext('2d'),
                    dataURL;
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                //callback(dataURL);
                canvas = null;
                resolve(dataURL);
            };
            img.src = url;
        });
    }

    samplePdf() {
        let doc = new jsPDF();
        {

            let x = 0, cnt = 0;
            for (let i = 0; i < 4; i++) {

                this.convertToDataURLviaCanvas('assets/img/mars.png', "image/jpeg")
                    .then(base64Img => {
                        x++;
                        cnt++;
                        let m = 0;
                        if (x == 1)
                            m = 20;
                        else
                            m = 155;

                        doc.addImage(base64Img, 'JPEG', 10, m, 190, 120);

                        //doc.addImage(base64Img, 'JPEG', 10, m, 190, 120);
                        if (x == 2) {
                            doc.addPage();
                            x = 0;
                        }

                        if (cnt == 4)
                            doc.save('demo.pdf');

                    })
            }
        }




    }

    ExportPDF() {
        console.log('download start');

        let doc = this.getJSPDFdata();
        //this.arrEOL.push({
        //    url: 'assets/img/chart.png',
        //    fileName: "fileName 1"
        //}, {
        //        url: 'assets/img/chart.png',
        //        fileName: "fileName 2"
        //    }, {
        //        url: 'assets/img/chart.png',
        //        fileName: "fileName 3"
        //    }, {
        //        url: 'assets/img/chart.png',
        //        fileName: "fileName 4"
        //    });


        let x = 0, cnt = 0, len = this.arrEOL.length;
        if (len > 0) {
            
            doc.addPage();
            doc.text(10, 9, "EOL CAMERA IMAGES");
        }
        else
            doc.save('BirthCertificate.pdf');


        for (let i in this.arrEOL) {

            this.convertToDataURLviaCanvas(this.arrEOL[i].url, "image/jpeg")
                .then(base64Img => {
                    x++;
                    cnt++;

                    let m = 0;
                    if (x == 1)
                        m = 20;
                    else
                        m = 155;

                    doc.text(10, m - 1, this.arrEOL[i].fileName);
                    doc.addImage(base64Img, 'JPEG', 10, m, 190, 120, {
                        startY: doc.autoTableEndPosY() + 13 + 13,
                    }, 'fast');
                    let ss = doc.internal.pageSize.height;
                    if (x == 2) {
                        doc.addPage();
                        x = 0;
                    }

                    if (cnt == len)
                        doc.save('BirthCertificate.pdf');

                })
        }

        // doc.save("BirthCertificate.pdf");

        // $("#modal_EOLCamera").css('display', 'block');


        //counter = 29;
        // yposition = doc.autoTableEndPosY() + counter;

        //doc.addHTML(document.getElementById('modal_EOLCamera_body'), 0, this.dd + 3,
        //    {
        //        useOverflow: false,
        //        background: '#fff',
        //        startY: doc.autoTableEndPosY() + 29
        //    }, function () {
        //        doc.save("BirthCertificate.pdf");
        //    });
    }

    getJSPDFdata() {
        let doc = new jsPDF();
        {
            doc.setTextColor(0);
            doc.setFontSize(16);
            doc.setFontSize(16);
            doc.text("Model Details", 90, 8);
            doc.setTextColor(0);
            doc.setFontSize(12);
            doc.cell(5, 10, 200, 8, " ");
            doc.text("VIN :-", 30, 16);
            doc.setTextColor(0, 158, 211);
            doc.text("" + this.LS.get("VIN_S") + "", 42, 16);
            doc.setTextColor(0);
            doc.text("Sequence Number :-", 70, 16)
            doc.setTextColor(0, 158, 211);
            doc.text("" + this.LS.get("SequenceNumber_I") + "", 110, 16);
            doc.setTextColor(0);
            doc.text("ModuleType :-", 143, 16)
            doc.setTextColor(0, 158, 211);
            doc.text("" + this.LS.get("ModuleType_S") + "", 171, 16);
            doc.setTextColor(0);
            //doc.text("VIN: " + this.LS.get("VIN_S") + " SequenceNumber: " + this.LS.get("SequenceNumber_I") + " ModuleType: " + this.LS.get("ModuleType_S"), 5, 15);
        }

        let yposition = doc.autoTableEndPosY() + 25
        let counter = 10;
        //....BroadcastInfo
        {
            let C_BroadcastInfo = [
                { title: "Module Type", dataKey: "moduleType_S" },
                { title: "Sequence Number", dataKey: "sequenceNumber_I" },
                { title: "Creation Time", dataKey: "creation_time" },
                { title: "Status Code", dataKey: "statusCode_S" }
            ];
            doc.setFontSize(14);
            doc.text(5, yposition, "Broadcast Info"),
                doc.autoTable(C_BroadcastInfo, this.M_BroadcastInfo, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [8, 150, 197],
                    },
                    margin: {
                        left: 5,
                        right: 5

                    },
                    startY: doc.autoTableEndPosY() + 27
                });
            if (this.M_BroadcastInfo.length == 0)
                this.addEmptyRow(doc);
        }
        //....SA Message
        {
            let C_SA = [
                { title: "VIN", dataKey: "viN_S" },
                { title: "Sequence Number", dataKey: "sequenceNumber_I" },
                { title: "Creation Time", dataKey: "creation_time" },
                { title: "Module Type", dataKey: "moduleType_S" }

            ];
            if (this.M_SA.length == 0)
                counter = 13;
            yposition = doc.autoTableEndPosY() + counter;

            doc.text(5, yposition, "SA Message"),
                doc.autoTable(C_SA, this.M_SA, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [8, 150, 197],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,
                });
            if (this.M_SA.length == 0)
                this.addEmptyRow(doc);
        }
        //....SR Message
        {
            let C_SR = [
                { title: "VIN", dataKey: "viN_S" },
                { title: "Sequence Number", dataKey: "sequenceNumber_I" },
                { title: "SrSend Time", dataKey: "srSendTime_T" },
                { title: "Parts Changed", dataKey: "partschanged" }

            ];
            if (this.M_SR.length == 0)
                counter = 13;
            yposition = doc.autoTableEndPosY() + counter;

            doc.text(5, yposition, "SR Message"),
                doc.autoTable(C_SR, this.M_SR, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [8, 150, 197],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,
                });
            if (this.M_SR.length == 0)
                this.addEmptyRow(doc);
        }
        //....Components Used
        {
            var C_ComponentsUsed = [
                { title: "PartNumber", dataKey: "partNumber_S" },
                { title: "Part_desc", dataKey: "part_desc_S" },
                { title: "PartQty", dataKey: "partQty_S" }

            ];

            if (this.M_ComponentsUsed.length == 0)
                counter = 13;
            yposition = doc.autoTableEndPosY() + counter;

            doc.text(5, yposition, "Components Used"),
                doc.autoTable(C_ComponentsUsed, this.M_ComponentsUsed, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [8, 197, 97],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,
                });
            if (this.M_ComponentsUsed.length == 0)
                this.addEmptyRow(doc);
        }
        //....Machine Controlled Operations
        {
            let C_MCO = [
                { title: "Equipment Id", dataKey: "equipment_Id_S" },
                { title: "Torque Type", dataKey: "torque_Type_S" },
                { title: "Quantity", dataKey: "qty" }
            ];
            let C_MCO1 = [
                { title: "Equipment Id", dataKey: "equipment_Id_S" },
                { title: "Torque Angle", dataKey: "torqueAngle_S" },
                { title: "Torque Status", dataKey: "torqueStatus_S" },
                { title: "Torque Value", dataKey: "torqueValue_S" },
                { title: "Torque Type", dataKey: "torque_Type_S" },
                { title: "Creation Time", dataKey: "creation_time" }

            ];
            if (this.M_MCO.length == 0)
                counter = 13;

            yposition = doc.autoTableEndPosY() + counter;


            doc.text(5, yposition, "Machine Controlled Operations");
            for (let i in this.M_MCO) {
                let styleV = doc.autoTableEndPosY() + 0;
                if (i == "0") {
                    styleV = doc.autoTableEndPosY() + counter + 3;
                }
                let pTempData = [];
                pTempData.push({
                    equipment_Id_S: this.M_MCO[i].equipment_Id_S,
                    torque_Type_S: this.M_MCO[i].torque_Type_S,
                    qty: this.M_MCO[i].qty
                });

                doc.autoTable(C_MCO, pTempData, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [9, 53, 165],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: styleV,
                });

                let Cdata = this.M_MCO[i].childdetails;

                doc.autoTable(C_MCO1, Cdata, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [120, 125, 130],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + 0,


                });

            }

            if (this.M_MCO.length == 0) {
                doc.autoTable(C_MCO, this.M_MCO, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [9, 53, 165],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,
                });

                this.addEmptyRow(doc);
            }
        }
        if (this.modelType == 'X83') {

            //....KIT
            {
                let C_KIT = [
                    { title: "Station", dataKey: "station_S" },
                    { title: "Type", dataKey: "wI_Type_S" },
                    { title: "WI Description", dataKey: "wI_Description_S" },
                    { title: "Start Time", dataKey: "start_Time_T" },
                    { title: "User Name", dataKey: "user_Name_S" },
                    { title: "Short Description", dataKey: "short_Description_S" },
                    { title: "Input", dataKey: "input_S" }
                ];

                if (this.M_KIT.length == 0)
                    counter = 13;
                yposition = doc.autoTableEndPosY() + counter;

                doc.text(5, yposition, "Kit Station Details"),
                    doc.autoTable(C_KIT, this.M_KIT, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [9, 53, 165],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: doc.autoTableEndPosY() + counter + 3,

                    });
                if (this.M_KIT.length == 0)
                    this.addEmptyRow(doc);
            }
            //....HoodLatch
            {
                let C_HoodLatch = [
                    { title: "Station", dataKey: "station_S" },
                    { title: "Type", dataKey: "wI_Type_S" },
                    { title: "WI Description", dataKey: "wI_Description_S" },
                    { title: "Start Time", dataKey: "start_Time_T" },
                    { title: "User Name", dataKey: "user_Name_S" },
                    { title: "Short Description", dataKey: "short_Description_S" },
                    { title: "Input", dataKey: "input_S" }
                ];

                if (this.M_KIT.length == 0)
                    counter = 13;
                yposition = doc.autoTableEndPosY() + counter;

                doc.text(5, yposition, "Hood Latch Station Details"),
                    doc.autoTable(C_HoodLatch, this.M_HoodLatch, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [9, 53, 165],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: doc.autoTableEndPosY() + counter + 3,

                    });
                if (this.M_KIT.length == 0)
                    this.addEmptyRow(doc);
            }
        }
        //....Labels Printed
        {
            let C_LabelsPrinted = [
                { title: "Line", dataKey: "line_S" },
                { title: "VIN", dataKey: "viN_S" },
                { title: "Sequence", dataKey: "sequence_S" },
                { title: "Start Time", dataKey: "start_Time_T" },
                { title: "User Name", dataKey: "user_Name_S" }
            ];

            if (this.M_LabelPrinting.length == 0)
                counter = 13;
            yposition = doc.autoTableEndPosY() + counter;

            doc.text(5, yposition, "Labels Printed"),
                doc.autoTable(C_LabelsPrinted, this.M_LabelPrinting, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [9, 53, 165],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,

                });
            if (this.M_LabelPrinting.length == 0)
                this.addEmptyRow(doc);
        }
        //....Retriggers
        {
            let C_Retrigger = [
                { title: "Station", dataKey: "station_S" },
                { title: "Type", dataKey: "wI_Type_S" },
                { title: "Description", dataKey: "wI_Description_S" },
                { title: "Start Time", dataKey: "start_Time_T" },
                { title: "User Name", dataKey: "user_Name_S" }
            ];

            if (this.M_Retriggers.length == 0)
                counter = 13;
            yposition = doc.autoTableEndPosY() + counter;

            doc.text(5, yposition, "Retriggers"),
                doc.autoTable(C_Retrigger, this.M_Retriggers, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [165, 63, 9],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,

                });
            if (this.M_Retriggers.length == 0)
                this.addEmptyRow(doc);
        }
        //....Badge Scan
        {
            let C_BadgeScan = [
                { title: "Station", dataKey: "station_S" },
                { title: "Type", dataKey: "wI_Type_S" },
                { title: "Description", dataKey: "wI_Description_S" },
                { title: "Start Time", dataKey: "start_Time_T" },
                { title: "User Name", dataKey: "user_Name_S" }
            ];

            if (this.M_BadgeScans.length == 0)
                counter = 13;
            yposition = doc.autoTableEndPosY() + counter;

            doc.text(5, yposition, "Badge Scan"),
                doc.autoTable(C_BadgeScan, this.M_BadgeScans, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [165, 63, 9],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,

                });
            if (this.M_BadgeScans.length == 0)
                this.addEmptyRow(doc);
        }
        //...BYpass
        {
            let C_BYpass = [
                { title: "Station", dataKey: "station_S" },
                { title: "WI Type", dataKey: "wi_Type_S" },
                { title: "WI Description", dataKey: "wi_Description_S" },
                { title: "Start Time", dataKey: "start_Time_T" },
                { title: "User Name", dataKey: "user_Name_S" }
            ];

            if (this.M_BYpass.length == 0)
                counter = 13;
            yposition = doc.autoTableEndPosY() + counter;

            doc.text(5, yposition, "Bypass"),
                doc.autoTable(C_BYpass, this.M_BYpass, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [165, 9, 70],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3

                });
            if (this.M_BYpass.length == 0)
                this.addEmptyRow(doc);
        }
        //...EOL Test Completion
        {
            let C_EOLCOmpletion = [
                { title: "Station", dataKey: "station_S" },
                { title: "Type", dataKey: "wI_Type_S" },
                { title: "WI Description", dataKey: "wI_Description_S" },
                { title: "Start Time", dataKey: "start_Time_T" },
                { title: "End Time", dataKey: "end_Time_T" },
                { title: "User Name", dataKey: "user_Name_S" },
                { title: "Short Description", dataKey: "short_Description_S" },
                { title: "Input", dataKey: "input_S" },
                { title: "WI Sequence", dataKey: "wI_Seq_S" },
            ];

            if (this.M_EOLCOmpletion.length == 0)
                counter = 13;
            yposition = doc.autoTableEndPosY() + counter;

            doc.text(5, yposition, "EOL Test Completion"),
                doc.autoTable(C_EOLCOmpletion, this.M_EOLCOmpletion, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [9, 165, 155],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3

                });
            if (this.M_EOLCOmpletion.length == 0)
                this.addEmptyRow(doc);
        }
        //...Shipments
        {
            let C_Shipments = [
                { title: "ID", dataKey: "ID" },
                { title: "Status", dataKey: "Status" },
                { title: "Shipped", dataKey: "Shipped" },
                { title: "Truck", dataKey: "Truck" },
                { title: "Rack ID", dataKey: "RackID" },
                { title: "Trans4M ID", dataKey: "Trans4M ID" },
            ];

            if (this.M_Shipments.length == 0)
                counter = 13;
            yposition = doc.autoTableEndPosY() + counter;

            doc.text(5, yposition, "Shipments"),
                doc.autoTable(C_Shipments, this.M_Shipments, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [183, 178, 9],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3

                });
            if (this.M_Shipments.length == 0)
                this.addEmptyRow(doc);
        }
        //....AGS Genealogy....ECM Genealogy....Carrier Genealogy
        if (this.modelType == 'X83') {
            //....AGS Genealogy
            {

                let C_AGS1 = [
                    { title: "Scanned Value", dataKey: "input_S" },
                    { title: "Station", dataKey: "station_S" },
                    { title: "Type", dataKey: "wI_Type_S" },
                    { title: "Description", dataKey: "wI_Description_S" },
                    { title: "Start Time", dataKey: "start_Time_T" },
                    { title: "User Name", dataKey: "user_Name_S" }
                ];

                let C_AGS2 = [
                    { title: "WIP Number", dataKey: "wiP_Number_S" },
                    { title: "Part Number", dataKey: "part_Number_S" },
                    { title: "User Name", dataKey: "user_Name_S" },
                    { title: "Station", dataKey: "station_S" },
                    { title: "Updated Station", dataKey: "updated_Station_S" },
                    { title: "Updated User", dataKey: "updated_User_S" }
                ];

                let C_AGS3 = [
                    { title: "Station", dataKey: "station_S" },
                    { title: "Type", dataKey: "wI_Type_S" },
                    { title: "Description", dataKey: "wI_Description_S" },
                    { title: "Start Time", dataKey: "start_Time_T" },
                    { title: "End Time", datakey: "end_Time_T" },
                    { title: "User Name", dataKey: "user_Name_S" }
                ];

                if (this.M_AGS1.length == 0)
                    counter = 13;
                else
                    counter = 13;
                yposition = doc.autoTableEndPosY() + counter;

                doc.text(5, yposition, "AGS Genealogy"),
                    doc.autoTable(C_AGS1, this.M_AGS1, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [8, 150, 197],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: doc.autoTableEndPosY() + counter + 3,

                    });
                if (this.M_AGS1.length == 0)
                    this.addEmptyRow(doc);

                if (this.M_AGS2.length == 0)
                    counter = 13;
                else
                    counter = 10;
                yposition = doc.autoTableEndPosY() + counter;
                doc.autoTable(C_AGS2, this.M_AGS2, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [8, 150, 197],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,

                });
                if (this.M_AGS2.length == 0)
                    this.addEmptyRow(doc);


                if (this.M_AGS3.length == 0)
                    counter = 13;
                else
                    counter = 10;
                yposition = doc.autoTableEndPosY() + counter;
                doc.autoTable(C_AGS3, this.M_AGS3, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [8, 150, 197],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,

                });
                if (this.M_AGS3.length == 0)
                    this.addEmptyRow(doc);
            }
            //....ECM Genealogy
            {

                let C_ECM1 = [
                    { title: "Scanned Value", dataKey: "input_S" },
                    { title: "Station", dataKey: "station_S" },
                    { title: "Type", dataKey: "wI_Type_S" },
                    { title: "Description", dataKey: "wI_Description_S" },
                    { title: "Start Time", dataKey: "start_Time_T" },
                    { title: "User Name", dataKey: "user_Name_S" }
                ];
                let C_ECM2 = [
                    { title: "Scanned Value", dataKey: "input_S" },
                    { title: "Station", dataKey: "station_S" },
                    { title: "Type", dataKey: "wI_Type_S" },
                    { title: "Description", dataKey: "wI_Description_S" },
                    { title: "Start Time", dataKey: "start_Time_T" },
                    { title: "User Name", dataKey: "user_Name_S" }
                ];
                let C_ECM3 = [
                    { title: "Scanned Value", dataKey: "input_S" },
                    { title: "Station", dataKey: "station_S" },
                    { title: "Type", dataKey: "wI_Type_S" },
                    { title: "Description", dataKey: "wI_Description_S" },
                    { title: "Start Time", dataKey: "start_Time_T" },
                    { title: "User Name", dataKey: "user_Name_S" }
                ];



                if (this.M_ECM1.length == 0)
                    counter = 13;
                else
                    counter = 10;
                yposition = doc.autoTableEndPosY() + counter;

                doc.text(5, yposition, "ECM Genealogy"),
                    doc.autoTable(C_ECM1, this.M_ECM1, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [8, 197, 97],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: doc.autoTableEndPosY() + counter + 3,

                    });

                if (this.M_ECM1.length == 0)
                    this.addEmptyRow(doc);


                if (this.M_ECM2.length == 0)
                    counter = 13;
                else
                    counter = 10;
                yposition = doc.autoTableEndPosY() + counter;

                doc.autoTable(C_ECM2, this.M_ECM2, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [8, 197, 97],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,

                });
                if (this.M_ECM2.length == 0)
                    this.addEmptyRow(doc);


                if (this.M_ECM3.length == 0)
                    counter = 13;
                else
                    counter = 10;
                yposition = doc.autoTableEndPosY() + counter;

                doc.autoTable(C_ECM3, this.M_ECM3, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [8, 197, 97],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,

                });
                if (this.M_ECM3.length == 0)
                    this.addEmptyRow(doc);
            }
            //....Carrier parent Genealogy
            {

                let C_Carrier1 = [
                    { title: "Input", dataKey: "input_S" },
                    { title: "Station", dataKey: "station_S" },
                    { title: "Type", dataKey: "wI_Type_S" },
                    { title: "Description", dataKey: "wI_Description_S" },
                    { title: "Start Time", dataKey: "start_Time_T" },
                    { title: "User Name", dataKey: "user_Name_S" }
                ];


                if (this.M_Carrier.length == 0)
                    counter = 13;
                else
                    counter = 10;
                yposition = doc.autoTableEndPosY() + counter;

                doc.text(5, yposition, "Carrier Genealogy"),
                    doc.autoTable(C_Carrier1, this.M_Carrier, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [9, 53, 165],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: doc.autoTableEndPosY() + counter + 3,

                    });

                if (this.M_Carrier.length == 0)
                    this.addEmptyRow(doc);
            }
            //....Carrier child Genealogy
            {

                let C_Carrier2 = [
                    { title: "Sequence Number", dataKey: "sequence_S" },
                    { title: "Station", dataKey: "station_S" },
                    { title: "Type", dataKey: "wI_Type_S" },
                    { title: "Description", dataKey: "wI_Description_S" },
                    { title: "Short Description", dataKey: "short_Description_S" },
                    { title: "Input", dataKey: "input_S" },
                    { title: "Start Time", dataKey: "start_Time_T" },
                    { title: "User Name", dataKey: "user_Name_S" }
                ];


                if (this.M_Carrier.length == 0)
                    counter = 13;
                else
                    counter = 10;
                yposition = doc.autoTableEndPosY() + counter;

                doc.text(5, yposition, ""),
                    doc.autoTable(C_Carrier2, this.M_Carrier2, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [9, 53, 165],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: doc.autoTableEndPosY() + counter + 3,

                    });

                if (this.M_Carrier.length == 0)
                    this.addEmptyRow(doc);
            }
        }
        //....GRILLE genealogy //....Torque Data
        if (this.modelType == 'X79') {
            //....GRILLE genealogy
            {
                let C_GRILLE = [
                    { title: "Station", dataKey: "station_S" },
                    { title: "Quantity", dataKey: "qty" }
                ];

                let C_GRILLE1 = [
                    { title: "Station", dataKey: "station_S" },
                    { title: "Type", dataKey: "wI_Type_S" },
                    { title: "WI Description", dataKey: "wI_Description_S" },
                    { title: "Start Time", dataKey: "start_Time_T" },
                    { title: "End Time", dataKey: "end_Time_T" },
                    { title: "User Name", dataKey: "user_Name_S" },
                    { title: "Short Description", dataKey: "short_Description_S" },
                    { title: "Input", dataKey: "input_S" },
                    { title: "WI Sequence", dataKey: "wI_Seq_S" }

                ];
                if (this.M_GRILLE.length == 0)
                    counter = 13;

                yposition = doc.autoTableEndPosY() + counter;


                doc.text(5, yposition, "GRILLE Genealogy");
                for (let i in this.M_GRILLE) {
                    let styleV = doc.autoTableEndPosY() + 0;
                    if (i == "0") {
                        styleV = doc.autoTableEndPosY() + counter + 3;
                    }
                    let pTempData = [];
                    pTempData.push({
                        station_S: this.M_GRILLE[i].station_S,
                        qty: this.M_GRILLE[i].qty
                    });

                    doc.autoTable(C_GRILLE, pTempData, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [226, 85, 13],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: styleV,
                    });

                    let Cdata = this.M_GRILLE[i].childdetails;

                    doc.autoTable(C_GRILLE1, Cdata, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [120, 125, 130],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: doc.autoTableEndPosY() + 0,


                    });

                    //if (this.M_GRILLE[i].childdetails.length == 0)
                    //    this.addEmptyRow(doc);

                }

                if (this.M_GRILLE.length == 0) {
                    doc.autoTable(C_GRILLE, this.M_GRILLE, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [226, 85, 13],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: doc.autoTableEndPosY() + counter + 3,
                    });

                    this.addEmptyRow(doc);
                }
            }
            //....Torque Data
            {
                let C_Torque = [
                    { title: "Station", dataKey: "station_S" },
                    { title: "Quantity", dataKey: "qty" }
                ];

                let C_Torque1 = [
                    { title: "Equipment Id", dataKey: "equipment_Id_S" },
                    { title: "Torque Angle", dataKey: "torqueAngle_S" },
                    { title: "Torque Status", dataKey: "torqueStatus_S" },
                    { title: "Torque Value", dataKey: "torqueValue_S" },
                    { title: "Torque Type", dataKey: "torque_Type_S" },
                    { title: "Creation Time", dataKey: "creation_time" }

                ];
                if (this.M_Torque.length == 0)
                    counter = 13;

                yposition = doc.autoTableEndPosY() + counter;


                doc.text(5, yposition, "Torque Data");
                for (let i in this.M_Torque) {
                    let styleV = doc.autoTableEndPosY() + 0;
                    if (i == "0") {
                        styleV = doc.autoTableEndPosY() + counter + 3;
                    }
                    let pTempData = [];
                    pTempData.push({
                        station_S: this.M_Torque[i].station_S,
                        qty: this.M_Torque[i].qty
                    });

                    doc.autoTable(C_Torque, pTempData, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [4, 230, 4],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: styleV,
                    });

                    let Cdata = this.M_Torque[i].childdetails;

                    doc.autoTable(C_Torque1, Cdata, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [120, 125, 130],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: doc.autoTableEndPosY() + 0,


                    });

                }

                if (this.M_Torque.length == 0) {
                    doc.autoTable(C_Torque, this.M_Torque, {
                        theme: 'grid', // 'striped', 'grid' or 'plain' 
                        styles: {
                            overflow: 'linebreak',
                        },
                        headerStyles: {
                            columnWidth: 'wrap',
                            fillColor: [4, 230, 4],
                        },
                        margin: {
                            left: 5,
                            right: 5
                        },
                        startY: doc.autoTableEndPosY() + counter + 3,
                    });

                    this.addEmptyRow(doc);
                }
            }
        }
        //....Operator Genealogy
        {

            let C_Operator = [
                { title: "Station", dataKey: "station_S" },
                { title: "Badge ID", dataKey: "user_Name_S" },
                { title: "User Name", dataKey: "first_name" }
            ];


            if (this.M_Operator.length == 0)
                counter = 13;
            else
                counter = 13;
            yposition = doc.autoTableEndPosY() + counter;

            doc.text(5, yposition, "Operator Genealogy"),
                doc.autoTable(C_Operator, this.M_Operator, {
                    theme: 'grid', // 'striped', 'grid' or 'plain' 
                    styles: {
                        overflow: 'linebreak',
                    },
                    headerStyles: {
                        columnWidth: 'wrap',
                        fillColor: [230, 4, 178],
                    },
                    margin: {
                        left: 5,
                        right: 5
                    },
                    startY: doc.autoTableEndPosY() + counter + 3,

                });

            if (this.M_Operator.length == 0)
                this.addEmptyRow(doc);
        }
        //EOL Camera Images
        //{
        //    counter = 29;
        //    yposition = doc.autoTableEndPosY() + counter;

        //    doc.text(5, yposition, "EOL Camera Images");
        //}
        //doc.addHTML(document.getElementById('id2'), function () {
        //    doc.save('web.pdf');
        //});
        //  doc.text(5, this.yposition, "EOl Camera 567")

        this.yposition = doc.autoTableEndPosY() + counter;;
        return doc;
    }

    addEmptyRow(doc) {
        doc.text(90, doc.autoTableEndPosY() + 5, "No Data Found")
        doc.cell(5, doc.autoTableEndPosY(), 200, 7, ' ');
    }

    getData(FilterData: any) {

        this.birthCertificateService.getBirthCertificate(FilterData).subscribe(response => {
            console.log(response);
            this.SetData(response);
        })
    }

    SetData(response) {
        let temp = "";
        for (let i in response) {

            if (response[i].value == "BroadcastInfo") {
                this.M_BroadcastInfo.push({
                    moduleType_S: response[i].moduleType_S == null ? "" : response[i].moduleType_S,
                    sequenceNumber_I: response[i].sequenceNumber_I == null ? "" : response[i].sequenceNumber_I,
                    creation_time: response[i].creation_time == null ? "" : moment(response[i].creation_time).format("D/M/YYYY hh:mm:ss A"),
                    statusCode_S: response[i].statusCode_S == null ? "" : response[i].statusCode_S
                })
            }
            else if (response[i].value == "SA") {
                this.M_SA.push({
                    viN_S: response[i].viN_S == null ? "" : response[i].viN_S,
                    sequenceNumber_I: response[i].sequenceNumber_I == null ? "" : response[i].sequenceNumber_I,
                    creation_time: response[i].creation_time == null ? "" : moment(response[i].creation_time).format("D/M/YYYY hh:mm:ss A"),
                    messageType_S: response[i].messageType_S == null ? "" : response[i].messageType_S
                })
            }
            else if (response[i].value == "SR") {
                this.M_SR.push({
                    viN_S: response[i].viN_S == null ? "" : response[i].viN_S,
                    sequenceNumber_I: response[i].sequenceNumber_I == null ? "" : response[i].sequenceNumber_I,
                    srSendTime_T: response[i].srSendTime_T == null ? "" : moment(response[i].srSendTime_T).format("D/M/YYYY hh:mm:ss A"),
                    partschanged: 'none'

                })
            }
            else if (response[i].value == "ComponentsUsed") {
                this.M_ComponentsUsed.push({
                    partNumber_S: response[i].partNumber_S == null ? "" : response[i].partNumber_S,
                    part_desc_S: response[i].part_desc_S == null ? "" : response[i].part_desc_S,
                    partQty_S: response[i].partQty_S == null ? "" : response[i].partQty_S
                })
            }
            else if (response[i].value == "MachineControlledOperations") {

                if (response[i].class == "Parent") {
                    this.arrTemp = [];
                    for (let j in response) {
                        if (response[j].value == "MachineControlledOperations") {
                            if (response[j].class == "Child" && response[j].equipment_Id_S == response[i].equipment_Id_S) {
                                this.arrTemp.push({
                                    equipment_Id_S: response[j].equipment_Id_S == null ? "" : response[j].equipment_Id_S,
                                    torqueAngle_S: response[j].torqueAngle_S == null ? "" : response[j].torqueAngle_S,
                                    torqueStatus_S: response[j].torqueStatus_S == null ? "" : response[j].torqueStatus_S,
                                    torqueValue_S: response[j].torqueValue_S == null ? "" : response[j].torqueValue_S,
                                    torque_Type_S: response[j].torque_Type_S == null ? "" : response[j].torque_Type_S,
                                    creation_time: response[j].creation_time == null ? "" : moment(response[j].creation_time).format("D/M/YYYY hh:mm:ss A"),
                                    class: 'Child'
                                });

                            }
                        }
                    }
                    this.M_MCO.push({
                        equipment_Id_S: response[i].equipment_Id_S == null ? "" : response[i].equipment_Id_S,
                        torque_Type_S: response[i].torque_Type_S == null ? "" : response[i].torque_Type_S,
                        qty: response[i].qty == null ? "" : response[i].qty,
                        class: 'Parent',
                        childdetails: this.arrTemp
                    });
                }


            }
            else if (response[i].value == "KIT") {
                this.M_KIT.push({
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wI_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wI_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S,
                    short_Description_S: response[i].short_Description_S == null ? "" : response[i].short_Description_S,
                    input_S: response[i].input_S == null ? "" : response[i].input_S
                })
            }
            else if (response[i].value == "HoodLatch") {
                this.M_HoodLatch.push({
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wI_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wI_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S,
                    short_Description_S: response[i].short_Description_S == null ? "" : response[i].short_Description_S,
                    input_S: response[i].input_S == null ? "" : response[i].input_S
                })
            }
            else if (response[i].value == "LabelPrinting") {

                this.M_LabelPrinting.push({
                    line_S: response[i].line_S == null ? "" : response[i].line_S,
                    viN_S: response[i].viN_S == null ? "" : response[i].viN_S,
                    sequence_S: response[i].sequence_S == null ? "" : response[i].sequence_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S
                });
            }
            else if (response[i].value == "Retriggers") {
                this.M_Retriggers.push({
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wI_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wI_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S
                });
            }
            else if (response[i].value == "BadgeScans") {

                this.M_BadgeScans.push({
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wI_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wI_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S

                });
            }

            else if (response[i].value == "BYpass") {
                this.M_BYpass.push({
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wi_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wi_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S
                });
            }
            else if (response[i].value == "EOLCOmpletion") {
                this.M_EOLCOmpletion.push({
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wI_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wI_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    end_Time_T: response[i].end_Time_T == null ? "" : moment(response[i].end_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S,
                    short_Description_S: response[i].short_Description_S == null ? "" : response[i].short_Description_S,
                    input_S: response[i].input_S == null ? "" : response[i].input_S,
                    wI_Seq_S: response[i].wI_Seq_S == null ? "" : response[i].wI_Seq_S
                });
            }
            else if (response[i].value == "EOLCamera") {


                this.arrEOL.push({
                    fileName: response[i].fileName_S == null ? "" : response[i].fileName_S,
                    url: this.EOLCameraImagesPath + response[i].destinationFolder_S + '/' + response[i].fileName_S
                });

                this.M_EOLCamera = [];

                this.M_EOLCamera.push({
                    line_S: response[i].line_S == null ? "" : response[i].line_S,
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    stationID_I: response[i].stationID_I == null ? "" : response[i].stationID_I,
                    sequenceNumber_S: response[i].sequenceNumber_S == null ? "" : response[i].sequenceNumber_S,
                    sourceFolder_S: response[i].sourceFolder_S == null ? "" : response[i].sourceFolder_S,
                    destinationFolder_S: response[i].destinationFolder_S == null ? "" : response[i].destinationFolder_S,
                    fileName_S: this.arrEOL
                });

            }

            else if (response[i].value == "AGS1") {
                this.M_AGS1.push({
                    input_S: response[i].input_S == null ? "" : (response[i].input_S).substring((response[i].input_S).indexOf("-") + 1, (response[i].input_S).length),
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wI_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wI_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S
                });
            }
            else if (response[i].value == "AGS2") {

                this.M_AGS2.push({
                    wiP_Number_S: response[i].wiP_Number_S == null ? "" : response[i].wiP_Number_S,
                    part_Number_S: response[i].part_Number_S == null ? "" : response[i].part_Number_S,
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S,
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    updated_Station_S: response[i].updated_Station_S == null ? "" : response[i].updated_Station_S,
                    updated_User_S: response[i].updated_User_S == null ? "" : response[i].updated_User_S
                });

            }
            else if (response[i].value == "AGS3") {
                this.M_AGS3.push({
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wI_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wI_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    end_Time_T: response[i].end_Time_T == null ? "" : moment(response[i].end_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S
                });
            }
            else if (response[i].value == "ECM1") {
                this.M_ECM1.push({
                    input_S: response[i].input_S == null ? "" : (response[i].input_S).substring((response[i].input_S).indexOf("-") + 1, (response[i].input_S).length),
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wI_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wI_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S
                });
            }
            else if (response[i].value == "ECM2") {
                this.M_ECM2.push({
                    input_S: response[i].input_S == null ? "" : (response[i].input_S).substring((response[i].input_S).indexOf("-") + 1, (response[i].input_S).length),
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wI_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wI_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S
                });
            }
            else if (response[i].value == "ECM3") {
                this.M_ECM3.push({
                    input_S: response[i].input_S == null ? "" : (response[i].input_S).substring((response[i].input_S).indexOf("-") + 1, (response[i].input_S).length),
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wI_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wI_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S
                });
            }
            else if (response[i].value == "Carrier") {
                let input_val = (response[i].input_S).substring((response[i].input_S).indexOf("-") + 1, (response[i].input_S).length);
                this.M_Carrier.push({
                    input_S: response[i].input_S == null ? "" : (response[i].input_S).substring((response[i].input_S).indexOf("-") + 1, (response[i].input_S).length),
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    wI_Type_S: response[i].wI_Type_S == null ? "" : response[i].wI_Type_S,
                    wI_Description_S: response[i].wI_Description_S == null ? "" : response[i].wI_Description_S,
                    start_Time_T: response[i].start_Time_T == null ? "" : moment(response[i].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S
                });

                for (let c in response) {
                    if (response[c].value == "Carrier2") {
                        if (input_val == response[c].sequence_S) {
                            this.M_Carrier2.push({
                                sequence_S: response[c].sequence_S == null ? "" : response[c].sequence_S,
                                station_S: response[c].station_S == null ? "" : response[c].station_S,
                                wI_Type_S: response[c].wI_Type_S == null ? "" : response[c].wI_Type_S,
                                wI_Description_S: response[c].wI_Description_S == null ? "" : response[c].wI_Description_S,
                                short_Description_S: response[c].short_Description_S == null ? "" : response[c].short_Description_S,
                                input_S: response[c].input_S == null ? "" : response[c].input_S,
                                start_Time_T: response[c].start_Time_T == null ? "" : moment(response[c].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                                user_Name_S: response[c].user_Name_S == null ? "" : response[c].user_Name_S
                            });
                        }
                    }
                }
            }

            else if (response[i].value == "Operator") {
                this.M_Operator.push({
                    station_S: response[i].station_S == null ? "" : response[i].station_S,
                    user_Name_S: response[i].user_Name_S == null ? "" : response[i].user_Name_S,
                    first_name: response[i].first_name == null ? "" : response[i].first_name
                });
            }
            else if (response[i].value == "GRILLE") {
                if (response[i].class == "Parent") {
                    this.arrTemp = [];
                    let cnt = 0;
                    for (let j in response) {
                        if (response[j].value == "GRILLE") {
                            if (response[j].class == "Child" && response[j].station_S == response[i].station_S) {
                                //station_S,wI_Type_S,wI_Description_S,start_Time_T,end_Time_T,user_Name_S,short_Description_S,input_S
                                this.arrTemp.push({
                                    station_S: response[j].station_S == null ? "" : response[j].station_S,
                                    wI_Type_S: response[j].wI_Type_S == null ? "" : response[j].wI_Type_S,
                                    wI_Description_S: response[j].wI_Description_S == null ? "" : response[j].wI_Description_S,
                                    start_Time_T: response[j].start_Time_T == null ? "" : moment(response[j].start_Time_T).format("D/M/YYYY hh:mm:ss A"),
                                    end_Time_T: response[j].end_Time_T == null ? "" : moment(response[j].end_Time_T).format("D/M/YYYY hh:mm:ss A"),
                                    user_Name_S: response[j].user_Name_S == null ? "" : response[j].user_Name_S,
                                    short_Description_S: response[j].short_Description_S == null ? "" : response[j].short_Description_S,
                                    input_S: response[j].input_S == null ? "" : response[j].input_S,
                                    class: 'Child'
                                });
                                cnt++;
                            }
                        }
                    }
                    this.M_GRILLE.push({
                        station_S: response[i].station_S == null ? "" : response[i].station_S,
                        qty: cnt,
                        class: 'Parent',
                        childdetails: this.arrTemp
                    });
                }
            }
            else if (response[i].value == "Torque") {
                if (response[i].class == "Parent") {
                    this.arrTemp = [];
                    let cnt = 0;
                    for (let j in response) {
                        if (response[j].value == "Torque") {
                            if (response[j].class == "Child" && response[j].station_S == response[i].station_S) {

                                this.arrTemp.push({
                                    equipment_Id_S: response[j].equipment_Id_S == null ? "" : response[j].equipment_Id_S,
                                    torqueAngle_S: response[j].torqueAngle_S == null ? "" : response[j].torqueAngle_S,
                                    torqueStatus_S: response[j].torqueStatus_S == null ? "" : response[j].torqueStatus_S,
                                    torqueValue_S: response[j].torqueValue_S == null ? "" : response[j].torqueValue_S,
                                    torque_Type_S: response[j].torque_Type_S == null ? "" : response[j].torque_Type_S,
                                    creation_time: response[j].creation_time == null ? "" : moment(response[j].creation_time).format("D/M/YYYY hh:mm:ss A"),
                                    class: 'Child'
                                });
                                cnt++;
                            }
                        }
                    }
                    this.M_Torque.push({
                        station_S: response[i].station_S == null ? "" : response[i].station_S,
                        qty: cnt,
                        class: 'Parent',
                        childdetails: this.arrTemp
                    });
                }
            }
            //Shipments
            else if (response[i].value == "Shipments") {
                this.M_Shipments.push({
                    current_Sequence_S: response[i].current_Sequence_S == null ? "" : response[i].current_Sequence_S,
                    pallet_Id_S: response[i].pallet_Id_S == null ? "" : response[i].pallet_Id_S,
                    shipment_bld_order_S: response[i].shipment_bld_order_S == null ? "" : response[i].shipment_bld_order_S,
                    previous_Sequence_S: response[i].previous_Sequence_S == null ? "" : response[i].previous_Sequence_S,
                    quality_Status_S: response[i].quality_Status_S == null ? "" : response[i].quality_Status_S,
                    shipment_Status_S: response[i].shipment_Status_S == null ? "" : response[i].shipment_Status_S,
                    shipping_Time_T: response[i].shipping_Time_T == null ? "" : moment(response[i].shipping_Time_T).format("D/M/YYYY hh:mm:ss A"),
                    truck_Number_S: response[i].truck_Number_S == null ? "" : response[i].truck_Number_S,
                    lane_Number_S: response[i].lane_Number_S == null ? "" : response[i].lane_Number_S

                });
            }
        }

        console.log('this.M_BroadcastInfo'); console.log(this.M_BroadcastInfo);
        console.log('this.M_SA'); console.log(this.M_SA);
        console.log('this.M_SR'); console.log(this.M_SR);
        console.log('this.M_ComponentsUsed'); console.log(this.M_ComponentsUsed);
        console.log('this.M_MCO'); console.log(this.M_MCO);
        console.log('this.M_KIT'); console.log(this.M_KIT);
        console.log('this.M_HoodLatch'); console.log(this.M_HoodLatch);
        console.log('this.M_LabelPrinting'); console.log(this.M_LabelPrinting);
        console.log('this.M_Retriggers'); console.log(this.M_Retriggers);
        console.log('this.M_BadgeScans'); console.log(this.M_BadgeScans);
        console.log('this.M_BYpass'); console.log(this.M_BYpass);
        console.log('this.M_EOL_T_C'); console.log(this.M_EOL_T_C);
        console.log('this.M_Shipments'); console.log(this.M_Shipments);
        console.log('this.M_AGS1'); console.log(this.M_AGS1);
        console.log('this.M_AGS2'); console.log(this.M_AGS2);
        console.log('this.M_AGS3'); console.log(this.M_AGS3);
        console.log('this.M_ECM1'); console.log(this.M_ECM1);
        console.log('this.M_ECM2'); console.log(this.M_ECM2);
        console.log('this.M_ECM3'); console.log(this.M_ECM3);
        console.log('this.M_Carrier'); console.log(this.M_Carrier);
        console.log('this.M_Carrier2'); console.log(this.M_Carrier2);
        console.log('this.M_Operator'); console.log(this.M_Operator);
        console.log('this.M_EOLCOmpletion'); console.log(this.M_EOLCOmpletion);
        console.log('this.M_EOLCamera'); console.log(this.M_EOLCamera);
        console.log('this.M_GRILLE'); console.log(this.M_GRILLE);
        console.log('this.M_Torque'); console.log(this.M_Torque);

        this.masterService.StopLoader();
    }

    ngAfterViewInit() {

        $('#tbl_BirthCertificate').DataTable(
            {
                dom: 'Bfrtip',
                scrollX: true,
                buttons: [
                    'colvis',
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });

    }

    parentPanelToggle(e) {
        $(".panel-group").each(function (index) {
            let id = $(this).children('div').attr('data-id');
            if ($("#" + e).attr('class') == "fa fa-chevron-circle-down") {
                $("#" + id).removeClass('fa-chevron-circle-down');
                $("#" + id).addClass('fa-chevron-circle-up');
            }
            else {

                $("#" + id).removeClass('fa-chevron-circle-up');
                $("#" + id).addClass('fa-chevron-circle-down');
            }

        });

        //Expand All
        if ($("#i_ParrentToggle").attr('class') == "fa fa-chevron-circle-down") {
            $("#i_ParrentToggle").removeClass('fa-chevron-circle-down');
            $("#i_ParrentToggle").addClass('fa-chevron-circle-up');
            $('.panel-collapse:not(".in")').slideDown('slow');
        }
        else //Collapse All
        {
            $("#i_ParrentToggle").removeClass('fa-chevron-circle-up');
            $("#i_ParrentToggle").addClass('fa-chevron-circle-down');
            $('.panel-collapse:not(".in")').slideUp('slow');
        }
    }

    panelToggle(e) {

        $('html,body').animate({
            scrollTop: $("#" + e + "_group").offset().top - 150
        }, 500);


        console.log(e);
        if ($("#i_" + e).attr('class') == "fa fa-chevron-circle-down") {
            $("#i_" + e).removeClass('fa-chevron-circle-down');
            $("#i_" + e).addClass('fa-chevron-circle-up');
            $("#" + e).slideDown('slow');
        }
        else {
            $("#i_" + e).removeClass('fa-chevron-circle-up');
            $("#i_" + e).addClass('fa-chevron-circle-down');
            $("#" + e).slideUp('slow');
        }
        let flag = false;
        $(".panel-group").each(function (index) {
            let id = $(this).children('div').attr('data-id');
            if ($("#" + id).attr('class') == "fa fa-chevron-circle-up") {
                flag = true;
            }
        });
        if (flag) {
            $("#i_ParrentToggle").removeClass('fa-chevron-circle-down');
            $("#i_ParrentToggle").addClass('fa-chevron-circle-up');
        }
        else {
            $("#i_ParrentToggle").removeClass('fa-chevron-circle-up');
            $("#i_ParrentToggle").addClass('fa-chevron-circle-down');
        }

    }

    showchild(event, parentValue) {
        console.log(parentValue);
        $("[data-hide='" + parentValue + "']").slideToggle('slow');

        if ($(event.currentTarget).children('a').children('i').hasClass('fa-chevron-down')) {
            $(event.currentTarget).children('a').children('i').removeClass('fa-chevron-down')
            $(event.currentTarget).children('a').children('i').addClass('fa-chevron-up');
        }
        else {
            $(event.currentTarget).children('a').children('i').removeClass('fa-chevron-up')
            $(event.currentTarget).children('a').children('i').addClass('fa-chevron-down');
        }

        // alert('bhau');
    }

    ngAfterViewChecked() {

    }
    ngOnDestroy() {
        clearInterval(this.Filter_InInterval);
    }

}