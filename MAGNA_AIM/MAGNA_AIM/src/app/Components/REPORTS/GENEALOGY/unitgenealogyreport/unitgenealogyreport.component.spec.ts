import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitgenealogyreportComponent } from './unitgenealogyreport.component';

describe('UnitgenealogyreportComponent', () => {
  let component: UnitgenealogyreportComponent;
  let fixture: ComponentFixture<UnitgenealogyreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitgenealogyreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitgenealogyreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
