import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchBuildComponent } from './batch-build.component';

describe('BatchBuildComponent', () => {
  let component: BatchBuildComponent;
  let fixture: ComponentFixture<BatchBuildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchBuildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchBuildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
