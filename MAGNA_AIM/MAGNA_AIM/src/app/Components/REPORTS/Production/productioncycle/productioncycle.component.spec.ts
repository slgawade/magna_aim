import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductioncycleComponent } from './productioncycle.component';

describe('ProductioncycleComponent', () => {
  let component: ProductioncycleComponent;
  let fixture: ComponentFixture<ProductioncycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductioncycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductioncycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
