import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QualityscrapcountComponent } from './qualityscrapcount.component';

describe('QualityscrapcountComponent', () => {
  let component: QualityscrapcountComponent;
  let fixture: ComponentFixture<QualityscrapcountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QualityscrapcountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QualityscrapcountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
