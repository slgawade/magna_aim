import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BufferGrilleHeaderComponent } from './buffer-grille-header.component';

describe('BufferGrilleHeaderComponent', () => {
  let component: BufferGrilleHeaderComponent;
  let fixture: ComponentFixture<BufferGrilleHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BufferGrilleHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BufferGrilleHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
