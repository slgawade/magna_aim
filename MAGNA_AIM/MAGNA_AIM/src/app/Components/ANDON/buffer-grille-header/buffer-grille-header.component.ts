import { Component, OnInit } from '@angular/core';
import { AndonService } from '../../../Services/andon.service';
import { Http, URLSearchParams } from "@angular/http";
import { MasterService } from '../../../Services/Master.service';

import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var require: any;

@Component({
    selector: 'app-buffer-grille-header',
    templateUrl: './buffer-grille-header.component.html',
    styleUrls: ['./buffer-grille-header.component.css'],
    providers: [AndonService]
})
export class BufferGrilleHeaderComponent implements OnInit {
    GRILLE_HEADER_Interval: any;
    TIME_Interval: any;
    time: string;
    flag: boolean = false;
    G_BUFFER = 0; G_QUEUE = 0; G_LASTRECEIVED = 0; G_INSTALLPOINT = 0; G_LASTBUILD = 0; G_LASTSHIPPED = 0;
    G_1ST = 0; G_2ND = 0; G_FCA_1ST = 0; G_FCA_2ND = 0; G_LAST_MSG = 0;

    H_QUEUE = 0; H_BUFFER = 0; H_LASTRECEIVED = 0; H_INSTALLPOINT = 0; H_LASTBUILD = 0; H_LASTSHIPPED = 0;
    H_1ST = 0; H_2ND = 0; H_FCA_JPH = 0; H_AIM_JPH = 0; H_LAST_MSG = 0;

    constructor(private andonService: AndonService, private masterService: MasterService) { }

    ngOnInit() {
        this.masterService.StartLoader();
        this.getData();
        this.startInterval();
        this.TimeInterval();
    }
    getData() {
        let FilterParamData = new URLSearchParams();
        FilterParamData.append("mode", "GRILLE_HEADER");
        this.andonService.getAndonData(FilterParamData).subscribe(response => {
            console.log(response);
            this.flag = true;
           // console.log(moment(new Date()).format('hh:mm:ss a'));
            this.setData(response);
        });
    }
    setData(res) {
        for (let i in res) {
            {
                if (res[i].value == "G_QUEUE") {
                    this.G_QUEUE = res[i].count;
                    if (this.G_QUEUE <= 60) {
                        $("#G_QUEUE").removeClass();
                        $("#G_QUEUE").addClass('boxValue red');
                    }
                    if (this.G_QUEUE > 60 && this.G_QUEUE <= 80) {
                        $("#G_QUEUE").removeClass();
                        $("#G_QUEUE").addClass('boxValue yellow');
                    }
                    if (this.G_QUEUE > 80) {
                        $("#G_QUEUE").removeClass();
                        $("#G_QUEUE").addClass('boxValue green');
                    }
                }
                if (res[i].value == "G_BUFFER") {
                    this.G_BUFFER = res[i].count;
                    if (this.G_BUFFER <= 60) {
                        $("#G_BUFFER").removeClass();
                        $("#G_BUFFER").addClass('boxValue red');
                    }
                    if (this.G_BUFFER > 60 && this.G_BUFFER <= 80) {
                        $("#G_BUFFER").removeClass();
                        $("#G_BUFFER").addClass('boxValue yellow');
                    }
                    if (this.G_BUFFER > 80) {
                        $("#G_BUFFER").removeClass();
                        $("#G_BUFFER").addClass('boxValue green');
                    }
                }
                if (res[i].value == "G_LASTRECEIVED") {
                    this.G_LASTRECEIVED = res[i].count;
                }
                if (res[i].value == "G_INSTALLPOINT") {
                    this.G_INSTALLPOINT = res[i].count;
                }
                if (res[i].value == "G_LASTBUILD") {
                    this.G_LASTBUILD = res[i].count;
                }
                if (res[i].value == "G_LASTSHIPPED") {
                    this.G_LASTSHIPPED = res[i].count;
                }
                if (res[i].value == "G_1ST") {
                    this.G_1ST = res[i].count;
                }
                if (res[i].value == "G_2ND") {
                    this.G_2ND = res[i].count;
                }
                if (res[i].value == "G_FCA_1ST") {
                    this.G_FCA_1ST = res[i].count;
                }
                if (res[i].value == "G_FCA_2ND") {
                    this.G_FCA_2ND = res[i].count;
                }
                if (res[i].value == "G_LAST_MSG") {
                    this.G_LAST_MSG = res[i].count;
                }
            }
            {
                if (res[i].value == "H_QUEUE") {
                    this.H_QUEUE = res[i].count;
                    if (this.H_QUEUE <= 60) {
                        $("#H_QUEUE").removeClass();
                        $("#H_QUEUE").addClass('boxValue red');
                    }
                    if (this.H_QUEUE > 60 && this.H_QUEUE <= 80) {
                        $("#H_QUEUE").removeClass();
                        $("#H_QUEUE").addClass('boxValue yellow');
                    }
                    if (this.H_QUEUE > 80) {
                        $("#H_QUEUE").removeClass();
                        $("#H_QUEUE").addClass('boxValue green');
                    }
                }
                if (res[i].value == "H_BUFFER") {
                    this.H_BUFFER = res[i].count;
                    if (this.H_BUFFER <= 60) {
                        $("#H_BUFFER").removeClass();
                        $("#H_BUFFER").addClass('boxValue red');
                    }
                    if (this.H_BUFFER > 60 && this.H_BUFFER <= 80) {
                        $("#H_BUFFER").removeClass();
                        $("#H_BUFFER").addClass('boxValue yellow');
                    }
                    if (this.H_BUFFER > 81) {
                        $("#H_BUFFER").removeClass();
                        $("#H_BUFFER").addClass('boxValue green');
                    }
                }
                if (res[i].value == "H_LASTRECEIVED") {
                    this.H_LASTRECEIVED = res[i].count;
                }
                if (res[i].value == "H_INSTALLPOINT") {
                    this.H_INSTALLPOINT = res[i].count;
                }
                if (res[i].value == "H_LASTBUILD") {
                    this.H_LASTBUILD = res[i].count;
                }
                if (res[i].value == "H_LASTSHIPPED") {
                    this.H_LASTSHIPPED = res[i].count;
                }
                if (res[i].value == "H_1ST") {
                    this.H_1ST = res[i].count;
                }
                if (res[i].value == "H_2ND") {
                    this.H_2ND = res[i].count;
                }
                if (res[i].value == "H_FCA_JPH") {
                    this.H_FCA_JPH = res[i].count;
                }
                if (res[i].value == "H_AIM_JPH") {
                    this.H_AIM_JPH = res[i].count;
                }
                if (res[i].value == "H_LAST_MSG") {
                    this.H_LAST_MSG = res[i].count;
                }
            }
        }

        // console log GRILLE
        //{
        //    console.log("this.G_QUEUE"); console.log(this.G_QUEUE);
        //    console.log("this.G_BUFFER"); console.log(this.G_BUFFER);
        //    console.log("this.G_LASTRECEIVED"); console.log(this.G_LASTRECEIVED);
        //    console.log("this.G_INSTALLPOINT"); console.log(this.G_INSTALLPOINT);
        //    console.log("this.G_LASTBUILD"); console.log(this.G_LASTBUILD);
        //    console.log("this.G_LASTSHIPPED"); console.log(this.G_LASTSHIPPED);
        //    console.log("this.G_1ST"); console.log(this.G_1ST);
        //    console.log("this.G_2ND"); console.log(this.G_2ND);
        //    console.log("this.G_FCA_1ST"); console.log(this.G_FCA_1ST);
        //    console.log("this.G_FCA_2ND"); console.log(this.G_FCA_2ND);
        //    console.log("this.G_LAST_MSG"); console.log(this.G_LAST_MSG);
        //}
        // console log HEADER
        //{
        //    console.log("this.H_QUEUE"); console.log(this.H_QUEUE);
        //    console.log("this.H_BUFFER"); console.log(this.H_BUFFER);
        //    console.log("this.H_LASTRECEIVED"); console.log(this.H_LASTRECEIVED);
        //    console.log("this.H_INSTALLPOINT"); console.log(this.H_INSTALLPOINT);
        //    console.log("this.H_LASTBUILD"); console.log(this.H_LASTBUILD);
        //    console.log("this.H_LASTSHIPPED"); console.log(this.H_LASTSHIPPED);
        //    console.log("this.H_1ST"); console.log(this.H_1ST);
        //    console.log("this.H_2ND"); console.log(this.H_2ND);
        //    console.log("this.H_FCA_JPH"); console.log(this.H_FCA_JPH);
        //    console.log("this.H_AIM_JPH"); console.log(this.H_AIM_JPH);
        //    console.log("this.H_LAST_MSG"); console.log(this.H_LAST_MSG);
        //}
        console.log('this.flag'); console.log(this.flag);
        this.masterService.StopLoader();
    }
    startInterval() {

        this.GRILLE_HEADER_Interval = setInterval(() => {
            if (this.flag == true) {
                this.getData();
                this.flag = false;
            }
           
        }, 1400);

    }
    TimeInterval() {
        this.TIME_Interval = setInterval(() => {
            this.time = moment(new Date()).format('hh:mm:ss A');
        }, 1000);

    }
    ngOnDestroy() {
        clearInterval(this.GRILLE_HEADER_Interval);
        clearInterval(this.TIME_Interval);
    }

}
