import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrilleShippingComponent } from './grille-shipping.component';

describe('GrilleShippingComponent', () => {
  let component: GrilleShippingComponent;
  let fixture: ComponentFixture<GrilleShippingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrilleShippingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrilleShippingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
