import { Component, OnInit } from '@angular/core';
import { AndonService } from '../../../Services/andon.service';
import { Http, URLSearchParams } from "@angular/http";
import { MasterService } from '../../../Services/Master.service';

import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var require: any;

@Component({
    selector: 'app-grille-shipping',
    templateUrl: './grille-shipping.component.html',
    styleUrls: ['./grille-shipping.component.css'],
    providers: [AndonService]
})
export class GrilleShippingComponent implements OnInit {

    SHIP_Interval: any;

    time: string;
    flag: boolean = false;
    TRUCK: string = "TRUCK COMPLETE";
    rGRILLE: string = "0"; rHEADER: string = "0";
    bufferGRILLE: string = "0"; bufferHEADER: string = "0";
    statusHEADER: string = "0"; statusGRILLE: string = "0";
    H_Rack_Id_1: string = ""; H_Rack_Id_2: string = ""; H_Rack_Id_3: string = ""; H_TotalCount: string = "0";
    H_class_1: string = ""; H_class_2: string = ""; H_class_3: string = "";
    G_Rack_Id_1: string = ""; G_Rack_Id_2: string = ""; G_Rack_Id_3: string = ""; G_Rack_Id_4: string = ""; G_Rack_Id_5: string = ""; G_Rack_Id_6: string = ""; G_TotalCount: string = "0";
    G_class_1: string = ""; G_class_2: string = ""; G_class_3: string = ""; G_class_4: string = ""; G_class_5: string = ""; G_class_6: string = "";


    constructor(private andonService: AndonService, private masterService: MasterService) {
       // console.log(moment(new Date()).format('hh:mm:ss a'));
    }

    ngOnInit() {

        this.masterService.StartLoader();
        this.getData();
       // this.startInterval();
    }
    getData() {
       
        let FilterParamData = new URLSearchParams();
        FilterParamData.append("mode", "GRILLE_SHIPPING");
        this.andonService.getAndonData(FilterParamData).subscribe(response => {
             console.log(response);
            console.log(response.length);
            this.flag = true;
            this.setData(response);
            
        });
    }
    setData(res) {

        this.rGRILLE = "0"; this.rHEADER = "0";
        this.bufferGRILLE = "0"; this.bufferHEADER = "0";
        this.statusHEADER = "0"; this.statusGRILLE = "0";
        this.H_TotalCount = "0"; this.G_TotalCount = "0";

        this.H_Rack_Id_1 = ''; this.H_Rack_Id_2 = ''; this.H_Rack_Id_3 = '';
        this.H_class_1 = ''; this.H_class_2 = ''; this.H_class_3 = '';
        this.G_Rack_Id_1 = ''; this.G_Rack_Id_2 = ''; this.G_Rack_Id_3 = '';
        this.G_Rack_Id_4 = ''; this.G_Rack_Id_5 = ''; this.G_Rack_Id_6 = '';
        this.G_class_1 = ''; this.G_class_2 = ''; this.G_class_3 = '';
        this.G_class_4 = ''; this.G_class_5 = ''; this.G_class_6 = '';



        for (let i in res) {

            if (res[i].value == "HEADER") {

                this.rHEADER = res[i].rHEADER;
                this.H_TotalCount = res[i].totalCount;
                this.bufferHEADER = res[i].bufferHEADER;
                this.statusHEADER = res[i].statusCount;

              
            }
            else if (res[i].value == "R_HEADER") {
                if (this.H_Rack_Id_1 == '') {
                    this.H_Rack_Id_1 = res[i].h_Rack_Id_S;
                    this.H_class_1 = res[i].class;
                }
                else if (this.H_Rack_Id_2 == '') {
                    this.H_Rack_Id_2 = res[i].h_Rack_Id_S;
                    this.H_class_2 = res[i].class;
                }
                else if (this.H_Rack_Id_3 == '') {
                    this.H_Rack_Id_3 = res[i].h_Rack_Id_S;
                    this.H_class_3 = res[i].class;
                }
            }
            else if (res[i].value == "GRILLE") {
                this.rGRILLE = res[i].rGRILLE;
                this.G_TotalCount = res[i].totalCount;
                this.bufferGRILLE = res[i].bufferGRILLE;
                this.statusGRILLE = res[i].statusCount;
            }
            else if (res[i].value == "R_GRILLE") {

                if (this.G_Rack_Id_1 == '') {
                    this.G_Rack_Id_1 = res[i].g_Rack_Id_S;
                    this.G_class_1 = res[i].class;
                }
                else if (this.G_Rack_Id_2 == '') {
                    this.G_Rack_Id_2 = res[i].g_Rack_Id_S;
                    this.G_class_2 = res[i].class;
                }
                else if (this.G_Rack_Id_3 == '') {
                    this.G_Rack_Id_3 = res[i].g_Rack_Id_S;
                    this.G_class_3 = res[i].class;
                }
                else if (this.G_Rack_Id_4 == '') {
                    this.G_Rack_Id_4 = res[i].g_Rack_Id_S;
                    this.G_class_4 = res[i].class;
                }
                else if (this.G_Rack_Id_5 == '') {
                    this.G_Rack_Id_5 = res[i].g_Rack_Id_S;
                    this.G_class_5 = res[i].class;
                }
                else if (this.G_Rack_Id_6 == '') {
                    this.G_Rack_Id_6 = res[i].g_Rack_Id_S;
                    this.G_class_6 = res[i].class;
                }
            }
            else if (res[i].value == "TRUCK")
                this.TRUCK = res[i].truck; 
        }

      //  console.log('this.flag'); console.log(this.flag);
        this.masterService.StopLoader();
    }
    startInterval() {

        this.SHIP_Interval = setInterval(() => {
            if (this.flag == true) {
                this.getData();
                this.flag = false;
            }
        }, 1400);

    }

    ngOnDestroy() {
        clearInterval(this.SHIP_Interval);

    }

}
