import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BufferFemComponent } from './buffer-fem.component';

describe('BufferFemComponent', () => {
  let component: BufferFemComponent;
  let fixture: ComponentFixture<BufferFemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BufferFemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BufferFemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
