import { Component, OnInit } from '@angular/core';
import { AndonService } from '../../../Services/andon.service';
import { Http, URLSearchParams } from "@angular/http";
import { MasterService } from '../../../Services/Master.service';

import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var require: any;

@Component({
    selector: 'app-buffer-fem',
    templateUrl: './buffer-fem.component.html',
    styleUrls: ['./buffer-fem.component.css'],
    providers: [AndonService]
})
export class BufferFemComponent implements OnInit {

    FEM_Interval: any;
    TIME_Interval: any;
    time: string;
    flag: boolean = false;
    BUFFER: number=0; QUEUE: number=0; LASTRECEIVED: number=0; INSTALLPOINT: number=0; LASTLOADED: number=0; LASTSHIPPED : number=0;
    FEM_1ST: number=0; FEM_2ND: number=0; AAD_1ST: number=0; AAD_2ND: number=0; AGS_1ST: number=0; AGS_2ND: number=0;
    CARRIER_1ST: number = 0; CARRIER_2ND: number = 0; FCA_1ST: number = 0; FCA_2ND: number = 0;
    FCA_JPH: number = 0; AIM_JPH: number = 0; LAST_MSG: number = 0; SHIPMENTCOUNT: number = 0;


    constructor(private andonService: AndonService, private masterService: MasterService) {
        console.log(moment(new Date()).format('hh:mm:ss a'));
    }

    ngOnInit() {

        this.masterService.StartLoader();
        this.getData();
        this.startInterval();
        this.TimeInterval();
        
    }
    getData() {
        let FilterParamData = new URLSearchParams();
        FilterParamData.append("mode", "FEM");
        this.andonService.getAndonData(FilterParamData).subscribe(response => {
            console.log(response);
          //  console.log(moment(new Date()).format('hh:mm:ss a'));
            this.flag = true; 
            this.setData(response);
           
        });
    }
    setData(res) {
        for (let i in res) {
            if (res[i].value == "QUEUE") {
                this.QUEUE = res[i].count;
                if (this.QUEUE <= 60) {
                    $("#fem_QUEUE").removeClass();
                    $("#fem_QUEUE").addClass('boxValue red');
                }
                if (this.QUEUE > 60 && this.QUEUE <= 80) {
                    $("#fem_QUEUE").removeClass();
                    $("#fem_QUEUE").addClass('boxValue yellow');
                }
                if (this.QUEUE > 80) {
                    $("#fem_QUEUE").removeClass();
                    $("#fem_QUEUE").addClass('boxValue green');
                }
            }
            if (res[i].value == "BUFFER") {
                this.BUFFER = res[i].count;

                if (this.BUFFER <= 60) {
                    $("#fem_BUFFER").removeClass();
                    $("#fem_BUFFER").addClass('boxValue red');
                }
                if (this.BUFFER > 60 && this.BUFFER <= 80) {
                    $("#fem_BUFFER").removeClass();
                    $("#fem_BUFFER").addClass('boxValue yellow');
                }
                if (this.BUFFER > 80) {
                    $("#fem_BUFFER").removeClass();
                    $("#fem_BUFFER").addClass('boxValue green');
                }
            }
            if (res[i].value == "LASTRECEIVED") {
                this.LASTRECEIVED = res[i].count;
            }
            if (res[i].value == "INSTALLPOINT") {
                this.INSTALLPOINT = res[i].count;
            }
            if (res[i].value == "LASTLOADED") {
                this.LASTLOADED = res[i].count;
            }
            if (res[i].value == "LASTSHIPPED") {
                this.LASTSHIPPED  = res[i].count;
            }
            if (res[i].value == "FEM_1ST") {
                this.FEM_1ST = res[i].count;
            }
            if (res[i].value == "FEM_2ND") {
                this.FEM_2ND = res[i].count;
            }
            if (res[i].value == "AAD_1ST") {
                this.AAD_1ST = res[i].count;
            }
            if (res[i].value == "AAD_2ND") {
                this.AAD_2ND = res[i].count;
            }
            if (res[i].value == "AGS_1ST") {
                this.AGS_1ST = res[i].count;
            }
            if (res[i].value == "AGS_2ND") {
                this.AGS_2ND = res[i].count;
            }
            if (res[i].value == "FCA_1ST") {
                this.FCA_1ST = res[i].count;
            }
            if (res[i].value == "FCA_2ND") {
                this.FCA_2ND = res[i].count;
            }
            if (res[i].value == "CARRIER_1ST") {
                this.CARRIER_1ST = res[i].count;
            }
            if (res[i].value == "CARRIER_2ND") {
                this.CARRIER_2ND = res[i].count;
            }           
            if (res[i].value == "FCA_JPH") {
                this.FCA_JPH = res[i].count;
            }
            if (res[i].value == "AIM_JPH") {
                this.AIM_JPH = res[i].count;
            }
            if (res[i].value == "LAST_MSG") {
                this.LAST_MSG = res[i].count;
            }
            if (res[i].value == "SHIPMENTCOUNT") {
                this.SHIPMENTCOUNT = res[i].count;
            }
           
        }

        // console log
          {
            //console.log("this.QUEUE"); console.log(this.QUEUE);
            //console.log("this.BUFFER"); console.log(this.BUFFER);
            //console.log("this.LASTRECEIVED"); console.log(this.LASTRECEIVED);
            //console.log("this.INSTALLPOINT"); console.log(this.INSTALLPOINT);
            //console.log("this.LASTLOADED"); console.log(this.LASTLOADED);
            //console.log("this.LASTSHIPPED "); console.log(this.LASTSHIPPED);
            //console.log("this.FEM_1ST"); console.log(this.FEM_1ST);
            //console.log("this.FEM_2ND"); console.log(this.FEM_2ND);
            //console.log("this.AAD_1ST"); console.log(this.AAD_1ST);
            //console.log("this.AAD_2ND"); console.log(this.AAD_2ND);
            //console.log("this.AGS_1ST"); console.log(this.AGS_1ST);
            //console.log("this.AGS_2ND"); console.log(this.AGS_2ND);
            //console.log("this.FCA_1ST"); console.log(this.FCA_1ST);
            //console.log("this.FCA_2ND"); console.log(this.FCA_2ND);
            //console.log("this.CARRIER_1ST"); console.log(this.CARRIER_1ST);
            //console.log("this.CARRIER_2ND"); console.log(this.CARRIER_2ND);
            //console.log("this.FCA_JPH"); console.log(this.FCA_JPH);
            //console.log("this.AIM_JPH"); console.log(this.AIM_JPH);
            //console.log("this.LAST_MSG"); console.log(this.LAST_MSG);
            //console.log("this.SHIPMENTCOUNT"); console.log(this.SHIPMENTCOUNT);
        }
        console.log('this.flag'); console.log(this.flag);
        this.masterService.StopLoader();
    }
    startInterval() {
        
        this.FEM_Interval = setInterval(() => {
            if (this.flag == true) {
                this.getData();
                this.flag = false;
            }
        }, 1400);

    }
    TimeInterval() {
        this.TIME_Interval = setInterval(() => {
            this.time = moment(new Date()).format('hh:mm:ss A');
        }, 1000);

    }
    ngOnDestroy() {
        clearInterval(this.FEM_Interval);
        clearInterval(this.TIME_Interval);
    }
}
