﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, URLSearchParams } from "@angular/http";
import { AmChartsService } from "@amcharts/amcharts3-angular";
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var _: any;

@Component({
    selector: 'demo',
    templateUrl: `./demo.html`,
    //templateUrl: `./HtmlPage1.html`,
    //styleUrls: ['./test.css'],

})
export class demoComponent {
    private chart: any;
    datasource = [];
    FilterData: any;
    Filter_InInterval: any;

    constructor(private router: Router, private AmCharts: AmChartsService) {

    }
    ngOnInit() {
        this.chart = this.AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "marginRight": 70,
            "dataProvider": [{
                "country": "USA",
                "visits": 3025,
                "color": "#FF0F00"
            }, {
                "country": "China",
                "visits": 1882,
                "color": "#FF6600"
            }, {
                "country": "Japan",
                "visits": 1809,
                "color": "#FF9E01"
            }, {
                "country": "Germany",
                "visits": 1322,
                "color": "#FCD202"
            }, {
                "country": "UK",
                "visits": 1122,
                "color": "#F8FF01"
            }, {
                "country": "France",
                "visits": 1114,
                "color": "#B0DE09"
            }, {
                "country": "India",
                "visits": 984,
                "color": "#04D215"
            }, {
                "country": "Spain",
                "visits": 711,
                "color": "#0D8ECF"
            }, {
                "country": "Netherlands",
                "visits": 665,
                "color": "#0D52D1"
            }, {
                "country": "Russia",
                "visits": 580,
                "color": "#2A0CD0"
            }, {
                "country": "South Korea",
                "visits": 443,
                "color": "#8A0CCF"
            }, {
                "country": "Canada",
                "visits": 441,
                "color": "#CD0D74"
            }],
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left",
                "title": "Visitors from country"
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<b>[[category]]: [[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 45
            },
            "export": {
                "enabled": true,
                "fileName": "PLAN_VS_ACTUAL_" + moment(new Date()).format('DD-MM-YYYY_hh:mm a'),
                "menuReviver": function (config, li) {
                    // MODIFY ONLY IMAGE ITEMS
                    if (config.capture) {
                        var link = li.getElementsByTagName("a")[0];

                        // ADD ANOTHER CLICK HANDLER ON TOP TO CHANGE THE FILENAME
                        link.addEventListener("click", function (e) {
                            config.fileName = "PLAN_VS_ACTUAL_" + moment(new Date()).format('DD-MM-YYYY_hh:mm a');
                        });
                    }

                    // RETURN UNTOUCHED LIST ELEMENT
                    return li;
                },
                "menu": [{
                    "class": "export-main", "label": "Export",
                    "menu": [{
                        "label": "Download as ...",
                        "menu": ["PNG", "JPG", "SVG", "PDF"]
                    }, {
                        "label": "Save data ...",
                        "menu": ["CSV", "XLSX", "JSON"]
                    }]
                }]
            }
        });
    }
    ngAfterViewInit() {

        $('#example').DataTable(
            {
                dom: 'Bfrtip',
                scrollX: true,
                scrollY: '370px',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
    }
    ngAfterViewChecked() {

    }
    ngOnDestroy() {
        this.AmCharts.destroyChart(this.chart);
    }

}