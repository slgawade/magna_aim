﻿import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, URLSearchParams } from "@angular/http";
import { REPORTSService } from '../../Services/REPORTS/REPORTS.service';
import { MasterService } from '../../Services/Master.service';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var _: any;

@Component({
    selector: 'MODULE',
    templateUrl: `./MODULE.html`,
    styleUrls: ['./MODULE.css', '../Master/Master.css'],
    providers: [REPORTSService]

})
export class MODULEComponent {

    datasource = {};
    currentState: string = "";

    titleName: string = "";
    filterdata = [];
    Filter_InInterval: any;
    col_ORGANISATION = [];
    col_PLANT = [];
    col_SHOP = [];
    col_LINE = [];
    col_STATION = [];
    col_MACHINES = [];
    col_SHIFT = [];
    col_STATES = []; col_CHARTTYPE = [];

    constructor(private router: Router,
        private masterService: MasterService,
        private maintenanceService: REPORTSService,
        private daterangepickerOptions: DaterangepickerConfig,
        private cdr: ChangeDetectorRef) {

        this.cdr = cdr;
        this.masterService.loadScript('assets/js/filter.js');
       
        this.DatePickerOptions();

    }
    CollapseFilter() {
        $("#mySidenav").css('width', '0');
        $("#main").css('marginLeft', '0');
        $("#filterToggle").removeClass('fa-angle-left');
        $("#filterToggle").addClass('fa-angle-right');
    }
    ngOnInit() {
        this.masterService.getTitle().subscribe(data => {
            console.log("REPORTSComponent ngOnInit()");
            this.titleName = data as string;
           
            //this.CollapseFilter();
            this.cdr.detectChanges();

        });
        this.masterService.getChildData().subscribe(data => {
           
            console.log("getChild Datasource ");
            this.datasource = data;
        });
    }
    Itemclick(event) {
       
    }
    getFilterReportData()
    {
        console.log('getFilterReportData');
    }
    onkeypress(event) {
        console.log($(event.currentTarget).val());
        let ind, input, filter, ul, li, a, i;
        filter = $(event.currentTarget).val().toUpperCase();
        ul = document.getElementById($(event.currentTarget).attr('data-name'));
        li = ul.getElementsByTagName("li");
        for (i = 1; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.text.toUpperCase().indexOf(filter) > -1) {
                $(li[i]).css('display', 'block');
            } else {
                $(li[i]).css('display', 'none')

            }
        }
    }
    DatePickerOptions() {
        this.daterangepickerOptions.settings = {
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 3 Months': [moment().subtract(4, 'month'), moment()],
                'Last 6 Months': [moment().subtract(6, 'month'), moment()],
                'Last 12 Months': [moment().subtract(12, 'month'), moment()],
            }
        };
    }
    public dateInputs: any = [
        {
            start: moment().subtract(29, 'days'),
            end: moment()
        }
    ];
    private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
    }
    ngAfterViewInit() {
        let table = $('#tbl_Maintenance').DataTable({
            paginationType: 'full_numbers',
            destroy: true,
            displayLength: 10,
            dom: 'Bfrtip',
            data: [],
            responsive: true,
            scrollCollapse: true,
            scrollX: true,
            scrollY: '370px',
            fixedHeader: true,
            //fixedColumns: true,
            paging: true,
            columns: [
                { title: "ALARM CODE" },
                { title: "ALARM DECRIPTION" },
                { title: "ALARM START TIME" },
                { title: "ALARM END TIME" },
                { title: "DOWN TIME" },
                { title: "SHIFT" },
                {
                    title: "ORGANISATION",
                    visible: false,
                },
                {
                    title: "PLANT",
                    visible: false,
                },
                {
                    title: "SHOP",
                    visible: false,
                },
                {
                    title: "LINE",
                    visible: false,
                },
                {
                    title: "STATION",
                    visible: false,
                },
                {
                    title: "EQUIPMENT",
                    visible: false,
                }
            ],

            buttons: [
                'colvis',
                'copyHtml5',
                'print',
                {
                    extend: 'csvHtml5',
                    filename: 'AlarmHistory_csv_' + moment(new Date()).format('DD-MM-YYYY_hh.mm a')
                },
                {
                    extend: 'pdfHtml5',
                    filename: 'AlarmHistory_pdf_' + moment(new Date()).format('DD-MM-YYYY_hh.mm a'),
                    orientation: 'landscape',
                },
                {
                    extend: 'excelHtml5',
                    filename: 'AlarmHistory_excel_' + moment(new Date()).format('DD-MM-YYYY_hh.mm a')
                }
            ]

        });
    }
    ngAfterViewChecked() {

    }
    ngOnDestroy() {

    }

}