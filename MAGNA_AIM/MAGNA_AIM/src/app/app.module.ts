import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';
import { MasterService } from './Services/Master.service';
import { FilterService } from './Services/filter.service';

//Amcharts
import { AmChartsModule } from "@amcharts/amcharts3-angular";
//daterange picker
import { Daterangepicker } from 'ng2-daterangepicker';
//cool local storage
import { LocalStorageModule } from 'angular-2-local-storage';
//import { CoolStorageModule } from 'angular2-cool-storage';
//Drag & Drop
import { DragulaModule } from 'ng2-dragula';


import { AppComponent, GlobalService } from './app.component';
import { LoginComponent } from './Components/Login/Login.component';
import { DashboardComponent } from './Components/Dashboard/Dashboard.component';
import { MasterComponent, HeaderComponent, FooterComponent } from './Components/Master/MASTER.component';
import { REPORTSComponent } from './Components/REPORTS/REPORTS.component';
import { VIN_DetailsComponent } from './Components/REPORTS/GENEALOGY/VIN_Details/VIN_Details.component';
import { BirthCertificateComponent } from './Components/REPORTS/GENEALOGY/BirthCertificate/BirthCertificate.component';
import { ShipmentAuditorComponent } from './Components/REPORTS/MATERIAL/ShipmentAuditor/ShipmentAuditor.component';
import { ShipmentDetailsComponent } from './Components/REPORTS/MATERIAL/ShipmentAuditor/shipment-details/shipment-details.component';

import { demoComponent } from './Components/demo/demo.component';
import { InventoryComponent } from './Components/REPORTS/MATERIAL/inventory/inventory.component';
import { UnshippedComponent } from './Components/REPORTS/MATERIAL/unshipped/unshipped.component';
import { QualityscrapcountComponent } from './Components/REPORTS/PRODUCTION/qualityscrapcount/qualityscrapcount.component';
import { UnitgenealogyreportComponent } from './Components/REPORTS/GENEALOGY/unitgenealogyreport/unitgenealogyreport.component';
import { ProductioncycleComponent } from './Components/REPORTS/PRODUCTION/productioncycle/productioncycle.component';
import { ShipmentFemComponent } from './Components/REPORTS/MATERIAL/SHIPMENT/shipment-fem/shipment-fem.component';
import { ShipmentGrilleComponent } from './Components/REPORTS/MATERIAL/SHIPMENT/shipment-grille/shipment-grille.component';
import { ShipmentHeaderComponent } from './Components/REPORTS/MATERIAL/SHIPMENT/shipment-header/shipment-header.component';
import { BypassComponent } from './Components/REPORTS/PRODUCTION/bypass/bypass.component';
import { BuildpartlookupComponent } from './Components/REPORTS/BROADCAST/buildpartlookup/buildpartlookup.component';
import { PphourComponent } from './Components/REPORTS/DASHBOARD/pphour/pphour.component';
import { PpoperatorComponent } from './Components/REPORTS/DASHBOARD/ppoperator/ppoperator.component';
import { QuerypartlookupComponent } from './Components/REPORTS/BROADCAST/querypartlookup/querypartlookup.component';
import { BufferFemComponent } from './Components/ANDON/buffer-fem/buffer-fem.component';
import { BufferGrilleHeaderComponent } from './Components/ANDON/buffer-grille-header/buffer-grille-header.component';
import { RawDataQueryComponent } from './Components/REPORTS/BROADCAST/raw-data-query/raw-data-query.component';
import { WipstatusComponent } from './Components/REPORTS/PRODUCTION/wipstatus/wipstatus.component';
import { PabdiscrepancyComponent } from './Components/REPORTS/BROADCAST/pabdiscrepancy/pabdiscrepancy.component';
import { PendingGqComponent } from './Components/REPORTS/BROADCAST/pending-gq/pending-gq.component';
import { PendingSrComponent } from './Components/REPORTS/BROADCAST/pending-sr/pending-sr.component';
import { BatchBuildComponent } from './Components/REPORTS/GENEALOGY/batch-build/batch-build.component';
import { QueueSummaryComponent } from './Components/REPORTS/BROADCAST/queue-summary/queue-summary.component';
import { ProductionSummaryComponent } from './Components/REPORTS/BROADCAST/production-summary/production-summary.component';
import { GrilleShippingComponent } from './Components/ANDON/grille-shipping/grille-shipping.component';
import { T1xxContainerDetailsComponent } from './Components/REPORTS/MATERIAL/t1xx-container-details/t1xx-container-details.component';




@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        MasterComponent,
        HeaderComponent,
        FooterComponent,
        DashboardComponent,
        demoComponent,
        REPORTSComponent,
        VIN_DetailsComponent,
        BirthCertificateComponent,
        ShipmentAuditorComponent,
        ShipmentDetailsComponent,
        InventoryComponent,
        UnshippedComponent,
        QualityscrapcountComponent,
        UnitgenealogyreportComponent,
        ProductioncycleComponent,
        BypassComponent,
        BuildpartlookupComponent,
        PphourComponent,
        PpoperatorComponent,
        QuerypartlookupComponent,
        BufferFemComponent,
        BufferGrilleHeaderComponent,
        RawDataQueryComponent,
        WipstatusComponent,
        ShipmentFemComponent,
        ShipmentGrilleComponent,
        ShipmentHeaderComponent,
        PabdiscrepancyComponent,
        PendingGqComponent,
        PendingSrComponent,
        BatchBuildComponent,
        QueueSummaryComponent,
        ProductionSummaryComponent,
        GrilleShippingComponent,
        T1xxContainerDetailsComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        AmChartsModule,
        Daterangepicker,
        LocalStorageModule.withConfig({
            prefix: 'my-app',
            storageType: 'localStorage'
        }),
        DragulaModule

        //CoolStorageModule
    ],
    providers: [MasterService, GlobalService, FilterService],
    bootstrap: [AppComponent]
})
export class AppModule { }
