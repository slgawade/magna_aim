﻿import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalService } from '../app.component';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var _: any;

@Injectable()
export class FilterService {
    api: string;
    constructor(private http: Http, private global: GlobalService) {
        this.api = global.Url + 'Filters/';
    }

    getLines(InputData) {
        return this.http.post(this.api, InputData).map(response =>
            response.json());
    }

}