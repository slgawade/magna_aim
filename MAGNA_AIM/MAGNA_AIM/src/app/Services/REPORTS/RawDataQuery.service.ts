﻿
import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalService } from '../../app.component';


@Injectable()
export class RawDataQueryService {
    api: string;
    constructor(private http: Http, private global: GlobalService) {
        this.api = global.Url + 'RawDataQuery';
    }

    getRawDataQuery(inputDetails) {
        return this.http.post(this.api, inputDetails).map(response =>
            response.json());
    }

}