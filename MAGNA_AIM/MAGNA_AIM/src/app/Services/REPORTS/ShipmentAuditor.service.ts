﻿
import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalService } from '../../app.component';


@Injectable()
export class ShipmentAuditorService {
    api: string;
    constructor(private http: Http, private global: GlobalService) {
        this.api = global.Url + 'ShipmentAuditor';
    }

    getShipmentAuditor(inputDetails) {
        return this.http.post(this.api, inputDetails).map(response =>
            response.json());
    }

}