﻿
import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalService } from '../../app.component';

@Injectable()
export class VIN_DetailsService {
    api: string;
    constructor(private http: Http, private global: GlobalService) {
        this.api = global.Url + 'VIN_Details/';
    }

    getVIN_Details(inputDetails) {
        //return this.http.get(this.api + inputDetails).map(response =>
        //    response.json());

        //let FilterParamData = new URLSearchParams();
        //FilterParamData.append("dynamic_where", inputDetails);

        return this.http.post(this.api, inputDetails).map(response =>
            response.json());
    }
    

}