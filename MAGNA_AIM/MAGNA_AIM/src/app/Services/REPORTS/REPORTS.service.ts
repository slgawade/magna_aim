﻿
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalService } from '../../app.component';

@Injectable()
export class REPORTSService {
    api: string;
    constructor(private http: Http, private global: GlobalService) {
        this.api = global.Url + 'REPORTS/';
    }

    getFilterColumn(filtermode, dynamicwhere) {
        return this.http.get(this.api + filtermode + '/' + dynamicwhere).map(response =>
            response.json());
    }

}