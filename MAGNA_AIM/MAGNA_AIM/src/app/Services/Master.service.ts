﻿
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalService } from '../app.component';
import { Subject } from 'rxjs/Rx';
declare var $: any;
declare var _: any;

@Injectable()
export class MasterService {
    api: string;
    VIN: string;
    Fdata: any;
    private dataObs$ = new Subject();
    private title$ = new Subject();
    private childData$ = new Subject();
    public filter_INFlag: boolean;
    public filter_OUTFlag: boolean;
    constructor(private http: Http, private global: GlobalService) {
        this.api = global.Url + 'MasterService/';
    }
    getData() {
        return this.dataObs$;
    }
    updateData(data: any) {
        this.dataObs$.next(data);
    }
    getChildData() {
        return this.childData$;
    }
    updateChildData(data: any) {
        this.childData$.next(data);
    }
    getTitle() {
        return this.title$;
    }
    updateTitle(data: string) {
        this.title$.next(data);
    }
    StartLoader() {
        $("#loaderDiv").css('display', 'block');
    }
    StopLoader() {
        $("#loaderDiv").css('display', 'none');
    }
    loadScript(url: string) {
        let flag = 0;
        let node = document.createElement('script');
        node.src = url;
        node.type = 'text/javascript';

        $('script[type]', document.getElementsByTagName('head')[0]).each(function () {
            if ($(this).attr('src') == url) {
                $(this).remove();
            }
        });

        document.getElementsByTagName('head')[0].appendChild(node);
    }
    clearTable(tableName)
    {
        if ($.fn.DataTable.isDataTable("#" + tableName)) {
            $('#' + tableName).DataTable().destroy();
            $('#' + tableName).empty();
        }
    }

    getImage() {
        console.log("getImage")
        return this.http.get("assets/img/menu.png").map(response =>
            response);
    }

    getLineData() {
        return this.http.get(this.global.Url + 'demo').map(response =>
            response.json());
    }
    getFTVPURL() {
        return this.global.FTVPurl;
    }
}