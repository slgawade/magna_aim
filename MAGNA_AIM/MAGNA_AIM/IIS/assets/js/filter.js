﻿$(function () {
    var Accordion = function (el, multiple) {
        this.el = el || {};
        // more then one submenu open?
        this.multiple = multiple || false;

        var dropdownlink = this.el.find('.dropdownlink');
        dropdownlink.on('click',
                        { el: this.el, multiple: this.multiple },
                        this.dropdown);
    };

    Accordion.prototype.dropdown = function (e) {
        //console.log('filter');
        var $el = e.data.el,
            $this = $(this),
            //this is the ul.submenuItems
            $next = $this.next();
        //debugger;
        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            //show only one menu at the same time
            $el.find('.submenuItems').not($next).slideUp().parent().removeClass('open');
        }
    }

    var accordion = new Accordion($('.accordion-menu'), false);
})

function openNav() {
    var ft = document.getElementById("filterToggle").className;
    if (ft == "fa fa-angle-right") {
        document.getElementById("mySidenav").style.width = "300px";
        document.getElementById("main").style.marginLeft = "300px";

        document.getElementById("filterToggle").classList.add('fa-angle-left');

        document.getElementById("filterToggle").classList.remove('fa-angle-right');
    }
    else {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";

        document.getElementById("filterToggle").classList.add('fa-angle-right');

        document.getElementById("filterToggle").classList.remove('fa-angle-left');
    }


}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}